<?xml version="1.0" encoding="UTF-8" ?>
<?oracle-xsl-mapper
  <!-- SPECIFICATION OF MAP SOURCES AND TARGETS, DO NOT MODIFY. -->
  <mapSources>
    <source type="WSDL">
      <schema location="oramds:/apps/irsmart/EnterpriseServiceLibrary/HR_COMMON/wsdl/IR_FUSION_INBOUND.1.wsdl"/>
      <rootElement name="Response" namespace="http://xmlns.oracle.com/Enterprise/Tools/schemas/IR_FUSION_STATUS_UPD_RESPONSE.V1"/>
    </source>
  </mapSources>
  <mapTargets>
    <target type="WSDL">
      <schema location="oramds:/apps/irsmart/FoundationServiceLibrary/ES_UTILITIES/wsdl/IRSGSubscriberService.wsdl"/>
      <rootElement name="IRSGPayLoadEventInfoResponse" namespace="http://model/events/schema/XxsgIfaceDetailEOResponse"/>
    </target>
  </mapTargets>
  <!-- GENERATED BY ORACLE XSL MAPPER 11.1.1.7.0(build 140919.1004.0161) AT [THU MAR 19 17:00:35 EDT 2015]. -->
?>
<xsl:stylesheet version="1.0"
                xmlns:client="http://xmlns.irco.com/IRSGSubscriberService"
                xmlns:getasyncp="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetAsyncParent"
                xmlns:IR_FUSION_STATUS_UPD_REQUEST.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/IR_FUSION_STATUS_UPD_REQUEST.V1"
                xmlns:bpws="http://schemas.xmlsoap.org/ws/2003/03/business-process/"
                xmlns:ircoutil="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility"
                xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20"
                xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:irsgreq="http://model/events/schema/XxsgIfaceDetailEO"
                xmlns:IR_FUSION_STATUS_UPD_RESPONSE.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/IR_FUSION_STATUS_UPD_RESPONSE.V1"
                xmlns:ns0="http://xmlns.irco.com/EnterpriseObjects/Core/Common/V1"
                xmlns:iruprec="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdatePhaseRec"
                xmlns:bpm="http://xmlns.oracle.com/bpmn20/extensions"
                xmlns:iruperr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdateErrorRec"
                xmlns:IR_FUSION_RESPONSE.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/IR_FUSION_RESPONSE.V1"
                xmlns:plnk="http://schemas.xmlsoap.org/ws/2003/05/partner-link/"
                xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/"
                xmlns:IR_FUSION_IN_REQUEST.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/IR_FUSION_IN_REQUEST.V1"
                xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"
                xmlns:ora="http://schemas.oracle.com/xpath/extension"
                xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator"
                xmlns:irsgresp="http://model/events/schema/XxsgIfaceDetailEOResponse"
                xmlns:ircifr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.CreateIfaceRecord"
                xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction"
                xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc"
                xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue"
                xmlns:tns="http://xmlns.oracle.com/Enterprise/Tools/services/IR_FUSION_INBOUND.1"
                xmlns:hwf="http://xmlns.oracle.com/bpel/workflow/xpath"
                xmlns:med="http://schemas.oracle.com/mediator/xpath"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:ids="http://xmlns.oracle.com/bpel/services/IdentityService/xpath"
                xmlns:irgifr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetIfaceRecord"
                xmlns:iruif="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdateIfaceStatus"
                xmlns:xdk="http://schemas.oracle.com/bpel/extension/xpath/function/xdk"
                xmlns:wsp="http://schemas.xmlsoap.org/ws/2002/12/policy"
                xmlns:irgsvurl="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetServiceURL"
                xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:ldap="http://schemas.oracle.com/xpath/extension/ldap"
                exclude-result-prefixes="xsi xsl IR_FUSION_STATUS_UPD_REQUEST.V1 IR_FUSION_STATUS_UPD_RESPONSE.V1 IR_FUSION_RESPONSE.V1 plnk soap IR_FUSION_IN_REQUEST.V1 wsdl tns wsp xsd client irsgreq ns0 irsgresp getasyncp bpws ircoutil xp20 bpel iruprec bpm iruperr ora socket ircifr mhdr oraext dvm hwf med ids irgifr iruif xdk irgsvurl xref ldap">
  <xsl:template match="/">
    <irsgresp:IRSGPayLoadEventInfoResponse>
      <irsgresp:DocumentId>
        <irsgresp:newValue>
          <xsl:attribute name="value">
            <xsl:text disable-output-escaping="no"></xsl:text>
          </xsl:attribute>
        </irsgresp:newValue>
      </irsgresp:DocumentId>
      <irsgresp:StatusCode>
        <irsgresp:newValue>
          <xsl:attribute name="value">
            <xsl:value-of select="/Response/ResponseCode"/>
          </xsl:attribute>
        </irsgresp:newValue>
      </irsgresp:StatusCode>
      <irsgresp:StatusMessage>
        <irsgresp:newValue>
          <xsl:attribute name="value">
            <xsl:value-of select="/Response/ResponseMessage"/>
          </xsl:attribute>
        </irsgresp:newValue>
      </irsgresp:StatusMessage>
      <irsgresp:StatusDetail>
        <irsgresp:newValue>
          <xsl:attribute name="value">
            <xsl:text disable-output-escaping="no"></xsl:text>
          </xsl:attribute>
        </irsgresp:newValue>
      </irsgresp:StatusDetail>
      <irsgresp:IRFault>
        <ns0:FaultMessageID>
          <xsl:text disable-output-escaping="no"></xsl:text>
        </ns0:FaultMessageID>
        <ns0:FaultMessageSource>
          <xsl:text disable-output-escaping="no"></xsl:text>
        </ns0:FaultMessageSource>
        <ns0:FaultReportedDateTime>
          <xsl:text disable-output-escaping="no"></xsl:text>
        </ns0:FaultReportedDateTime>
        <ns0:FaultContext>
          <ns0:HeaderMessageID>
            <xsl:text disable-output-escaping="no"></xsl:text>
          </ns0:HeaderMessageID>
          <ns0:HeaderMessageSource>
            <xsl:text disable-output-escaping="no"></xsl:text>
          </ns0:HeaderMessageSource>
          <ns0:HeaderCreatedDateTime>
            <xsl:text disable-output-escaping="no"></xsl:text>
          </ns0:HeaderCreatedDateTime>
        </ns0:FaultContext>
        <ns0:FaultFacts>
          <ns0:Category>
            <xsl:text disable-output-escaping="no"></xsl:text>
          </ns0:Category>
          <ns0:Type>
            <xsl:text disable-output-escaping="no"></xsl:text>
          </ns0:Type>
          <ns0:ErrorID>
            <xsl:text disable-output-escaping="no"></xsl:text>
          </ns0:ErrorID>
          <ns0:Severity>
            <xsl:text disable-output-escaping="no"></xsl:text>
          </ns0:Severity>
          <ns0:ShortDescription>
            <xsl:text disable-output-escaping="no"></xsl:text>
          </ns0:ShortDescription>
          <ns0:LongDescription>
            <xsl:text disable-output-escaping="no"></xsl:text>
          </ns0:LongDescription>
          <ns0:Code>
            <xsl:text disable-output-escaping="no"></xsl:text>
          </ns0:Code>
          <ns0:Summary>
            <xsl:text disable-output-escaping="no"></xsl:text>
          </ns0:Summary>
          <ns0:Trace>
            <xsl:text disable-output-escaping="no"></xsl:text>
          </ns0:Trace>
        </ns0:FaultFacts>
      </irsgresp:IRFault>
    </irsgresp:IRSGPayLoadEventInfoResponse>
  </xsl:template>
</xsl:stylesheet>
