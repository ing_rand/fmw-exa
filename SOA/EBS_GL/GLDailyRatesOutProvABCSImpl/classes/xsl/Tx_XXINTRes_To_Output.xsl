<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version="1.0" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:bpws="http://schemas.xmlsoap.org/ws/2003/03/business-process/" xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns3="http://xmlns.irco.com/EnterpriseObjects/Core/Common/V1" xmlns:bpm="http://xmlns.oracle.com/bpmn20/extensions" xmlns:plnk="http://schemas.xmlsoap.org/ws/2003/05/partner-link/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:ora="http://schemas.oracle.com/xpath/extension" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:inp1="http://www.ingerrand.com" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:hwf="http://xmlns.oracle.com/bpel/workflow/xpath" xmlns:med="http://schemas.oracle.com/mediator/xpath" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ids="http://xmlns.oracle.com/bpel/services/IdentityService/xpath" xmlns:tns="http://xmlns.oracle.com/XxintUtilities/XxintEventSoapCreateV1EsEbiz/CreateEvent" xmlns:xdk="http://schemas.oracle.com/bpel/extension/xpath/function/xdk" xmlns:ns2="http://xmlns.irco.com/GLDailyRatesOutProvABCSImpl" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:ns0="http://xmlns.irco.com/GLDailyRatesXXINTResponse" xmlns:ldap="http://schemas.oracle.com/xpath/extension/ldap" exclude-result-prefixes="xsi xsl wsdl inp1 tns xsd ns0 ns3 plnk ns2 xp20 bpws bpel bpm ora socket mhdr oraext dvm hwf med ids xdk xref ldap">
   <xsl:param name="Invoke_XXINTInboundService_execute_OutputVariable.reply"/>
   <xsl:template match="/">
      <ns2:GL_DAILY_RATES>
         <PARTNER_CODE>
            <xsl:value-of select="/ns0:GL_DAILY_RATES/PARTNER_CODE"/>
         </PARTNER_CODE>
         <INSTANCE_NAME>
            <xsl:value-of select="/ns0:GL_DAILY_RATES/INSTANCE_NAME"/>
         </INSTANCE_NAME>
         <STATUS_CODE>
            <xsl:value-of select="$Invoke_XXINTInboundService_execute_OutputVariable.reply/inp1:CreateEventMsgResponse/inp1:StatusCode"/>
         </STATUS_CODE>
         <STATUS_MESSAGE>
            <xsl:value-of select="$Invoke_XXINTInboundService_execute_OutputVariable.reply/inp1:CreateEventMsgResponse/inp1:StatusText"/>
         </STATUS_MESSAGE>
         <xsl:for-each select="/ns0:GL_DAILY_RATES/CURRENCY_CONVERSION">
            <CURRENCY_CONVERSION>
               <CONVERSION_TYPE>
                  <xsl:if test="CONVERSION_TYPE/@xsi:nil">
                     <xsl:attribute name="xsi:nil">
                        <xsl:value-of select="CONVERSION_TYPE/@xsi:nil"/>
                     </xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="CONVERSION_TYPE"/>
               </CONVERSION_TYPE>
               <CONVERSION_DATE>
                  <xsl:if test="CONVERSION_DATE/@xsi:nil">
                     <xsl:attribute name="xsi:nil">
                        <xsl:value-of select="CONVERSION_DATE/@xsi:nil"/>
                     </xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="CONVERSION_DATE"/>
               </CONVERSION_DATE>
               <FROM_CURRENCY>
                  <xsl:value-of select="FROM_CURRENCY"/>
               </FROM_CURRENCY>
               <TO_CURRENCY>
                  <xsl:value-of select="TO_CURRENCY"/>
               </TO_CURRENCY>
               <CONVERSION_RATE>
                  <xsl:value-of select="CONVERSION_RATE"/>
               </CONVERSION_RATE>
            </CURRENCY_CONVERSION>
         </xsl:for-each>
      </ns2:GL_DAILY_RATES>
   </xsl:template>
</xsl:stylesheet>
