<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version="1.0" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:bpws="http://schemas.xmlsoap.org/ws/2003/03/business-process/" xmlns:ircifr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.CreateIfaceRecord" xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:hwf="http://xmlns.oracle.com/bpel/workflow/xpath" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:med="http://schemas.oracle.com/mediator/xpath" xmlns:irgifr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetIfaceRecord" xmlns:ids="http://xmlns.oracle.com/bpel/services/IdentityService/xpath" xmlns:bpm="http://xmlns.oracle.com/bpmn20/extensions" xmlns:xdk="http://schemas.oracle.com/bpel/extension/xpath/function/xdk" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" xmlns:irgsvurl="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetServiceURL" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:ora="http://schemas.oracle.com/xpath/extension" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:ldap="http://schemas.oracle.com/xpath/extension/ldap" xmlns:obj1="http://xmlns.oracle.com/xdb/IRSOA" exclude-result-prefixes="xsi xsl xp20 bpws obj1 ircifr bpel mhdr oraext dvm hwf med irgifr ids bpm xdk xref irgsvurl ora socket ldap">
   <xsl:template match="/">
      <G_INPUT>
         <xsl:for-each select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE">
            <xsl:if test="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/ROUTING_ATTRIBUTE1 != &quot;&quot;">
               <PARAMETER>
                  <NAME>
                     <xsl:text disable-output-escaping="no">ROUTING_ATTRIBUTE1</xsl:text>
                  </NAME>
                  <VALUE>
                     <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/ROUTING_ATTRIBUTE1"/>
                  </VALUE>
               </PARAMETER>
            </xsl:if>
            <xsl:if test="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/ROUTING_ATTRIBUTE2 != ''">
               <PARAMETER>
                  <NAME>
                     <xsl:text disable-output-escaping="no">ROUTING_ATTRIBUTE2</xsl:text>
                  </NAME>
                  <VALUE>
                     <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/ROUTING_ATTRIBUTE2"/>
                  </VALUE>
               </PARAMETER>
            </xsl:if>
            <xsl:if test="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/ROUTING_ATTRIBUTE3 != ''">
               <PARAMETER>
                  <NAME>
                     <xsl:text disable-output-escaping="no">ROUTING_ATTRIBUTE3</xsl:text>
                  </NAME>
                  <VALUE>
                     <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/ROUTING_ATTRIBUTE3"/>
                  </VALUE>
               </PARAMETER>
            </xsl:if>
            <xsl:if test="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/ROUTING_ATTRIBUTE4 != ''">
               <PARAMETER>
                  <NAME>
                     <xsl:text disable-output-escaping="no">ROUTING_ATTRIBUTE4</xsl:text>
                  </NAME>
                  <VALUE>
                     <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/ROUTING_ATTRIBUTE4"/>
                  </VALUE>
               </PARAMETER>
            </xsl:if>
            <xsl:if test="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/ROUTING_ATTRIBUTE5 != ''">
               <PARAMETER>
                  <NAME>
                     <xsl:text disable-output-escaping="no">ROUTING_ATTRIBUTE5</xsl:text>
                  </NAME>
                  <VALUE>
                     <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/ROUTING_ATTRIBUTE5"/>
                  </VALUE>
               </PARAMETER>
            </xsl:if>
            <PARAMETER>
               <NAME>
                  <xsl:text disable-output-escaping="no">FMW_INSTANCE</xsl:text>
               </NAME>
               <VALUE>
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/FMW_INSTANCE"/>
               </VALUE>
            </PARAMETER>
         </xsl:for-each>
      </G_INPUT>
   </xsl:template>
</xsl:stylesheet>
