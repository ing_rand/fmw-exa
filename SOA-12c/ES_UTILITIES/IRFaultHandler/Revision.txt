RiceID : Common Service
Rice Name : IRFaultHandler
Composite Name : IRFaultHandler
Partition Name : ES_UTILITIES
Interaction Type : Asynchronous

Current Release
---------------
Type [Fix/Enhancement/Feature] : Enhancement
Revision : 2.1
Developer : Anuroop Katta
Code Repository [DIMEX/Sharepoint] : DIMEX - ESA-FMW-ESA >> SOA >> ES_UTILITIES
Comments : Added condition check for Error code in CatchAll Block.


Previous Release
---------------
Type [Fix/Enhancement/Feature] : Enhancement
Revision : 2.0
Developer : Anuroop Katta
Code Repository [DIMEX/Sharepoint] : DIMEX - ESA-FMW-ESA >> SOA >> ES_UTILITIES
Comments : Removed the Direct notification from IRFaultHandler and Enhanced IRFaultHandler to log the Errors in to Database.


Previous Release
---------------
Type [Fix/Enhancement/Feature] : Feature/Phase-3
Revision : 1.1.0.0
Developer : Mithilesh Kumar
Code Repository [DIMEX/Sharepoint] : DIMEX - ESA-FMW-ESA >> SOA >> ES_UTILITIES
Details :  
    -   Dynamic Routing Changes
    -   Added WS invocation to FMWToEBSErrorSubscriber
