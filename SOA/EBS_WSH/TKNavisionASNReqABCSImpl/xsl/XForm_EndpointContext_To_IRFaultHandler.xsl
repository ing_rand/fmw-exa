<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version="1.0" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:bpws="http://schemas.xmlsoap.org/ws/2003/03/business-process/" xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns3="http://xmnls.irco.com/EBSInstallItemSyncABCSImpResponse" xmlns:ns1="http://xmlns.irco.com/EnterpriseObjects/Core/Common/V1" xmlns:bpm="http://xmlns.oracle.com/bpmn20/extensions" xmlns:plnk="http://schemas.xmlsoap.org/ws/2003/05/partner-link/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:ora="http://schemas.oracle.com/xpath/extension" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:client="http://xmlns.irco.com/EBSInstallItemSyncABCSImpl" xmlns:ns2="http://www.ingerrand.com" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:hwf="http://xmlns.oracle.com/bpel/workflow/xpath" xmlns:med="http://schemas.oracle.com/mediator/xpath" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ids="http://xmlns.oracle.com/bpel/services/IdentityService/xpath" xmlns:xdk="http://schemas.oracle.com/bpel/extension/xpath/function/xdk" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:ns4="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:ldap="http://schemas.oracle.com/xpath/extension/ldap" exclude-result-prefixes="xsi xsl ns3 ns1 plnk wsdl client ns2 xsd ns4 xp20 bpws bpel bpm ora socket mhdr oraext dvm hwf med ids xdk xref ldap">
   <xsl:template match="/">
      <G_INPUT>
         <xsl:for-each select="/ns4:Security/ns4:UsernameToken/ns4:Username">
            <PARAMETER>
               <NAME>
                  <xsl:text disable-output-escaping="no">p_parent_service_name</xsl:text>
               </NAME>
               <VALUE>
                  <xsl:text disable-output-escaping="no">EBSASNProvABCSImpl</xsl:text>
               </VALUE>
            </PARAMETER>
            <PARAMETER>
               <NAME>
                  <xsl:text disable-output-escaping="no">p_child_service_name</xsl:text>
               </NAME>
               <VALUE>
                  <xsl:text disable-output-escaping="no">IRFaultHandler</xsl:text>
               </VALUE>
            </PARAMETER>
            <PARAMETER>
               <NAME>
                  <xsl:text disable-output-escaping="no">p_account_name</xsl:text>
          </NAME>                  
             <VALUE>
            <xsl:value-of select="."/>
          </VALUE>
        </PARAMETER>
      </xsl:for-each>
    </G_INPUT>
  </xsl:template>
</xsl:stylesheet>