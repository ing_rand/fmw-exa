<?xml version = '1.0' encoding = 'UTF-8'?>
<xsl:stylesheet version="1.0"
                xmlns:bpws="http://schemas.xmlsoap.org/ws/2003/03/business-process/"
                xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20"
                xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:ns0="http://xmlns.irco.com/EnterpriseObjects/Core/Common/V1"
                xmlns:bpm="http://xmlns.oracle.com/bpmn20/extensions"
                xmlns:plnk="http://schemas.xmlsoap.org/ws/2003/05/partner-link/"
                xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" 
                xmlns:ora="http://schemas.oracle.com/xpath/extension"
                xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator"
                xmlns:ns7="http://xmlns.irco.com/Utility/ApplicationArea"
                xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction"
                xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc"
                xmlns:hwf="http://xmlns.oracle.com/bpel/workflow/xpath"
                xmlns:med="http://schemas.oracle.com/mediator/xpath" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:ids="http://xmlns.oracle.com/bpel/services/IdentityService/xpath"
                xmlns:xdk="http://schemas.oracle.com/bpel/extension/xpath/function/xdk"
                xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
                xmlns:ns6="http://schemas.oracle.com/bpel/extension"
                xmlns:ns2="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
                xmlns:ns5="http://xmlns.oracle.com/ClubCarPavillion/ESTRNCustomerSyncProvABCSImpl/ESTRNCustomerSyncProvABCSImpl"
                xmlns:ldap="http://schemas.oracle.com/xpath/extension/ldap"
                exclude-result-prefixes="xsi xsl plnk wsdl ns7 ns3 ns4 xsd ns6 ns2 ns5 getasyncp bpws xp20 bpel iruprec bpm iruperr ora socket ircifr mhdr oraext dvm hwf med ids irgifr iruif xdk irgsvurl xref ldap"
                xmlns:oracle-xsl-mapper="http://www.oracle.com/xsl/mapper/schemas">
  <!--User Editing allowed BELOW this line - DO NOT DELETE THIS LINE-->
  <xsl:template match="/">
    <G_INPUT>
      <PARAMETER>
        <NAME>
          <xsl:text disable-output-escaping="no">p_parent_service_name</xsl:text>
        </NAME>
        <VALUE>
          <xsl:text disable-output-escaping="no">ARCSReconIdDownloadReqABCSImpl</xsl:text>
        </VALUE>
      </PARAMETER>
      <PARAMETER>
        <NAME>
          <xsl:text disable-output-escaping="no">p_child_service_name</xsl:text>
        </NAME>
        <VALUE>
          <xsl:text disable-output-escaping="no">ARCSDownloadFileClientService</xsl:text>
        </VALUE>
      </PARAMETER>
      <PARAMETER>
        <NAME>
          <xsl:text disable-output-escaping="no">p_account_name</xsl:text>
        </NAME>
        <VALUE>
          <xsl:value-of select="/ns2:Security/ns2:UsernameToken/ns2:Username"/>
        </VALUE>
      </PARAMETER>
    </G_INPUT>
  </xsl:template>
</xsl:stylesheet>