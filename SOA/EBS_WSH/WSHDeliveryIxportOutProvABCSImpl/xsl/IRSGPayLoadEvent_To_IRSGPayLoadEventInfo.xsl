<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version="1.0" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:bpws="http://schemas.xmlsoap.org/ws/2003/03/business-process/" xmlns:client="http://xmlns.irco.com/WSHDeliveryIxportOutProvABCSImpl" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:ns0="http://model/events/schema/XxsgIfaceDetailEO" xmlns:hwf="http://xmlns.oracle.com/bpel/workflow/xpath" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:med="http://schemas.oracle.com/mediator/xpath" xmlns:ids="http://xmlns.oracle.com/bpel/services/IdentityService/xpath" xmlns:bpm="http://xmlns.oracle.com/bpmn20/extensions" xmlns:xdk="http://schemas.oracle.com/bpel/extension/xpath/function/xdk" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" xmlns:irbpelx="http://xmlns.irco.com/BPEL/ir/foundation/soa/extensions/xpath" xmlns:plnk="http://schemas.xmlsoap.org/ws/2003/05/partner-link/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:ora="http://schemas.oracle.com/xpath/extension" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:ldap="http://schemas.oracle.com/xpath/extension/ldap" exclude-result-prefixes="xsi xsl ns0 xsd client plnk wsdl xp20 bpws mhdr bpel oraext dvm hwf med ids bpm xdk xref irbpelx ora socket ldap">
   <xsl:template match="/">
      <ns0:IRSGPayLoadEventInfo>
         <ns0:Attribute1>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:Attribute1/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:Attribute1/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:Attribute1/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:Attribute1/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:Attribute1>
         <ns0:Attribute10>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:Attribute10/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:Attribute10/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:Attribute10/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:Attribute10/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:Attribute10>
         <ns0:Attribute2>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:Attribute2/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:Attribute2/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:Attribute2/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:Attribute2/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:Attribute2>
         <ns0:Attribute3>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:Attribute3/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:Attribute3/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:Attribute3/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:Attribute3/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:Attribute3>
         <ns0:Attribute4>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:Attribute4/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:Attribute4/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:Attribute4/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:Attribute4/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:Attribute4>
         <ns0:Attribute5>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:Attribute5/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:Attribute5/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:Attribute5/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:Attribute5/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:Attribute5>
         <ns0:Attribute6>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:Attribute6/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:Attribute6/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:Attribute6/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:Attribute6/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:Attribute6>
         <ns0:Attribute7>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:Attribute7/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:Attribute7/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:Attribute7/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:Attribute7/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:Attribute7>
         <ns0:Attribute8>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:Attribute8/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:Attribute8/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:Attribute8/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:Attribute8/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:Attribute8>
         <ns0:Attribute9>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:Attribute9/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:Attribute9/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:Attribute9/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:Attribute9/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:Attribute9>
         <ns0:CreationDate>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:CreationDate/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:CreationDate/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:CreationDate/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:CreationDate/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:CreationDate>
         <ns0:DestSystemId>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:DestSystemId/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:DestSystemId/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:DestSystemId/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:DestSystemId/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:DestSystemId>
         <ns0:DocumentId>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:DocumentId/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:DocumentId/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:DocumentId/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:DocumentId/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:DocumentId>
         <ns0:EndDate>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:EndDate/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:EndDate/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:EndDate/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:EndDate/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:EndDate>
         <ns0:LastUpdateDate>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:LastUpdateDate/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:LastUpdateDate/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:LastUpdateDate/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:LastUpdateDate/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:LastUpdateDate>
         <ns0:ParentDocumentId>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:ParentDocumentId/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:ParentDocumentId/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:ParentDocumentId/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:ParentDocumentId/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:ParentDocumentId>
         <ns0:Priority>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:Priority/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:Priority/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:Priority/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:Priority/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:Priority>
         <ns0:RiceId>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:RiceId/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:RiceId/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:RiceId/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:RiceId/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:RiceId>
         <ns0:RoutingAttribute1>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:RoutingAttribute1/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:RoutingAttribute1/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:RoutingAttribute1/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:RoutingAttribute1/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:RoutingAttribute1>
         <ns0:RoutingAttribute2>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:RoutingAttribute2/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:RoutingAttribute2/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:RoutingAttribute2/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:RoutingAttribute2/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:RoutingAttribute2>
         <ns0:RoutingAttribute3>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:RoutingAttribute3/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:RoutingAttribute3/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:RoutingAttribute3/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:RoutingAttribute3/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:RoutingAttribute3>
         <ns0:RoutingAttribute4>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:RoutingAttribute4/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:RoutingAttribute4/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:RoutingAttribute4/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:RoutingAttribute4/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:RoutingAttribute4>
         <ns0:RoutingAttribute5>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:RoutingAttribute5/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:RoutingAttribute5/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:RoutingAttribute5/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:RoutingAttribute5/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:RoutingAttribute5>
         <ns0:SourceKey1>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:SourceKey1/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:SourceKey1/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:SourceKey1/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:SourceKey1/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:SourceKey1>
         <ns0:SourceKey2>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:SourceKey2/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:SourceKey2/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:SourceKey2/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:SourceKey2/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:SourceKey2>
         <ns0:SourceKey3>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:SourceKey3/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:SourceKey3/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:SourceKey3/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:SourceKey3/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:SourceKey3>
         <ns0:SourceKey4>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:SourceKey4/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:SourceKey4/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:SourceKey4/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:SourceKey4/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:SourceKey4>
         <ns0:SourceKey5>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:SourceKey5/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:SourceKey5/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:SourceKey5/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:SourceKey5/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:SourceKey5>
         <ns0:SourceSystemId>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:SourceSystemId/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:SourceSystemId/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:SourceSystemId/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:SourceSystemId/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:SourceSystemId>
         <ns0:StartDate>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:StartDate/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:StartDate/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:StartDate/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:StartDate/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:StartDate>
         <ns0:Status>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:Status/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:Status/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:Status/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:Status/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:Status>
         <ns0:TargetKey1>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:TargetKey1/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:TargetKey1/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:TargetKey1/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:TargetKey1/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:TargetKey1>
         <ns0:TargetKey2>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:TargetKey2/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:TargetKey2/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:TargetKey2/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:TargetKey2/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:TargetKey2>
         <ns0:TargetKey3>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:TargetKey3/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:TargetKey3/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:TargetKey3/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:TargetKey3/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:TargetKey3>
         <ns0:TargetKey4>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:TargetKey4/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:TargetKey4/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:TargetKey4/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:TargetKey4/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:TargetKey4>
         <ns0:TargetKey5>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:TargetKey5/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:TargetKey5/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:TargetKey5/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:TargetKey5/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:TargetKey5>
         <ns0:SOAPayloadEO.FileName>
            <ns0:newValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:SOAPayloadEO.FileName/ns0:newValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:SOAPayloadEO.FileName/ns0:newValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:newValue>
            <ns0:oldValue>
               <xsl:if test="/ns0:IRSGPayLoadEventInfo/ns0:SOAPayloadEO.FileName/ns0:oldValue/@value">
                  <xsl:attribute name="value">
                     <xsl:value-of select="/ns0:IRSGPayLoadEventInfo/ns0:SOAPayloadEO.FileName/ns0:oldValue/@value"/>
                  </xsl:attribute>
               </xsl:if>
            </ns0:oldValue>
         </ns0:SOAPayloadEO.FileName>
      </ns0:IRSGPayLoadEventInfo>
   </xsl:template>
</xsl:stylesheet>
