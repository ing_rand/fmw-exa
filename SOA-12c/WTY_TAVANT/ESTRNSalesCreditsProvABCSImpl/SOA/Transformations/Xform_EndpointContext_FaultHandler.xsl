<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
                xmlns:ircifr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.CreateIfaceRecord"
                xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction"
                xmlns:irgifr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetIfaceRecord"
                xmlns:iruif="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdateIfaceStatus"
                xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20"
                xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions"
                xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator"
                xmlns:oracle-xsl-mapper="http://www.oracle.com/xsl/mapper/schemas"
                xmlns:getasyncp="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetAsyncParent"
                xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue"
                xmlns:oraxsl="http://www.oracle.com/XSL/Transform/java"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:irgsvurl="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetServiceURL"
                xmlns:ns0="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
                exclude-result-prefixes="xsd oracle-xsl-mapper xsi xsl ns0 ircifr mhdr irgifr iruif oraext xp20 xref socket getasyncp dvm oraxsl irgsvurl"
                xmlns:plnk="http://docs.oasis-open.org/wsbpel/2.0/plnktype"
                xmlns:inp2="http://www.tavant.com/globalsync/commissionfetchresponse"
                xmlns:ns1="http://schemas.oracle.com/bpel/extension" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"
                xmlns:inp1="http://www.tavant.com/globalsync/commissionfetchrequest"
                xmlns:tns="http://xmlns.irco.com/ESTRNSalesCreditsProvABCSImpl">
  <oracle-xsl-mapper:schema>
    <!--SPECIFICATION OF MAP SOURCES AND TARGETS, DO NOT MODIFY.-->
    <oracle-xsl-mapper:mapSources>
      <oracle-xsl-mapper:source type="WSDL">
        <oracle-xsl-mapper:schema location="../WSDLs/ESTRNSalesCreditsProvABCSImpl.wsdl"/>
        <oracle-xsl-mapper:rootElement name="Security"
                                       namespace="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"/>
      </oracle-xsl-mapper:source>
    </oracle-xsl-mapper:mapSources>
    <oracle-xsl-mapper:mapTargets>
      <oracle-xsl-mapper:target type="WSDL">
        <oracle-xsl-mapper:schema location="../WSDLs/ESTRNSalesCreditsProvABCSImpl.wsdl"/>
        <oracle-xsl-mapper:rootElement name="G_INPUT" namespace=""/>
      </oracle-xsl-mapper:target>
    </oracle-xsl-mapper:mapTargets>
    <!--GENERATED BY ORACLE XSL MAPPER 12.2.1.0.0(XSLT Build 151013.0700.0085) AT [MON JAN 23 15:43:44 EST 2017].-->
  </oracle-xsl-mapper:schema>
  <!--User Editing allowed BELOW this line - DO NOT DELETE THIS LINE-->
  <xsl:template match="/">
    <G_INPUT>
      <PARAMETER>
        <NAME>
          <xsl:text disable-output-escaping="no">p_parent_service_name</xsl:text>
        </NAME>
        <VALUE>
          <xsl:text disable-output-escaping="no">ESTRNSalesCreditsProvABCSImpl</xsl:text>
        </VALUE>
      </PARAMETER>
      <PARAMETER>
        <NAME>
          <xsl:text disable-output-escaping="no">p_child_service_name</xsl:text>
        </NAME>
        <VALUE>
          <xsl:text disable-output-escaping="no">IRFaultHandler</xsl:text>
        </VALUE>
      </PARAMETER>
      <PARAMETER>
        <NAME>
          <xsl:text disable-output-escaping="no">p_account_name</xsl:text>
        </NAME>
        <VALUE>
          <xsl:value-of select="/ns0:Security/ns0:UsernameToken/ns0:Username"/>
        </VALUE>
      </PARAMETER>
    </G_INPUT>
  </xsl:template>
</xsl:stylesheet>
