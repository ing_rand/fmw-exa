<?xml version = '1.0' encoding = 'UTF-8'?>
<xsl:stylesheet version="1.0" xmlns:getasyncp="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetAsyncParent" xmlns:bpws="http://schemas.xmlsoap.org/ws/2003/03/business-process/" xmlns:ircoutil="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable" xmlns:tns="http://xmlns.irco.com/pcbpel/adapter/db/IRFaultHandler/XXIREMFErrorHandlingProc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns1="http://xmlns.irco.com/EnterpriseObjects/Core/Common/V1" xmlns:iruprec="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdatePhaseRec" xmlns:bpm="http://xmlns.oracle.com/bpmn20/extensions" xmlns:iruperr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdateErrorRec" xmlns:plnk="http://schemas.xmlsoap.org/ws/2003/05/partner-link/" xmlns:db="http://xmlns.irco.com/pcbpel/adapter/db/sp/XXIREMFErrorHandlingProc" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:ora="http://schemas.oracle.com/xpath/extension" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:ircifr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.CreateIfaceRecord" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:hwf="http://xmlns.oracle.com/bpel/workflow/xpath" xmlns:med="http://schemas.oracle.com/mediator/xpath" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ids="http://xmlns.oracle.com/bpel/services/IdentityService/xpath" xmlns:irgifr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetIfaceRecord" xmlns:client="http://xmlns.irco.com/IRFaultHandler/IREMFErrorHandler" xmlns:iruif="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdateIfaceStatus" xmlns:xdk="http://schemas.oracle.com/bpel/extension/xpath/function/xdk" xmlns:irgsvurl="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetServiceURL" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:ldap="http://schemas.oracle.com/xpath/extension/ldap" exclude-result-prefixes="xsi xsl ns1 plnk wsdl client xsd tns db getasyncp bpws ircoutil xp20 bpel iruprec bpm iruperr ora socket ircifr mhdr oraext dvm hwf med ids irgifr iruif xdk irgsvurl xref ldap">
   <xsl:template match="/">
      <db:InputParameters>
         <db:P_IN_LOG_RECORD>
            <db:COMPONENT_NAME>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:CompositeName"/>
            </db:COMPONENT_NAME>
            <db:BUSINESS_INTERFACE_NAME>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:BusinessInterfaceName"/>
            </db:BUSINESS_INTERFACE_NAME>
            <db:BUSINESS_TRACKING_ID>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:BusinessTrackingID"/>
            </db:BUSINESS_TRACKING_ID>
            <db:DOCUMENT_TYPE>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:DocumentType"/>
            </db:DOCUMENT_TYPE>
            <db:ENTERPRISE_CONTEXT_ID>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:EnterpriseContextID"/>
            </db:ENTERPRISE_CONTEXT_ID>
            <db:INSTANCE_ID>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:CompositeInstanceID"/>
            </db:INSTANCE_ID>
            <db:SOURECE_SYSTEM>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:SenderApplication"/>
            </db:SOURECE_SYSTEM>
            <db:TARGET_SYSTEM>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:DestinationApplication"/>
            </db:TARGET_SYSTEM>
            <db:FAULT_REPORTED_DATE_TIME>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultReportedDateTime"/>
            </db:FAULT_REPORTED_DATE_TIME>
            <db:FAULT_CATEGORY>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultFacts/ns1:Category"/>
            </db:FAULT_CATEGORY>
            <db:FAULT_TYPE>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultFacts/ns1:Type"/>
            </db:FAULT_TYPE>
            <db:FAULT_CODE>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultFacts/ns1:Code"/>
            </db:FAULT_CODE>
            <db:FAULT_SEVERITY>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultFacts/ns1:Severity"/>
            </db:FAULT_SEVERITY>
            <db:FAULT_SHORT_DESC>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultFacts/ns1:ShortDescription"/>
            </db:FAULT_SHORT_DESC>
            <db:FAULT_LONG_DESC>
               <xsl:value-of select="substring(/ns1:IRFault/ns1:FaultFacts/ns1:LongDescription,1.0,4000.0)"/>
            </db:FAULT_LONG_DESC>
            <db:FAULT_SUMMARY>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultFacts/ns1:Summary"/>
            </db:FAULT_SUMMARY>
            <db:FAULT_TRACE>
               <xsl:value-of select="substring(/ns1:IRFault/ns1:FaultFacts/ns1:Trace,1.0,4000.0)"/>
            </db:FAULT_TRACE>
            <db:FAULT_NOTE>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultFacts/ns1:Note"/>
            </db:FAULT_NOTE>
            <db:BUSINESS_KEY_NAME>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:BusinessKeys/ns1:Key/ns1:Name"/>
            </db:BUSINESS_KEY_NAME>
            <db:BUSINESS_KEY_VALUE>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:BusinessKeys/ns1:Key/ns1:Value"/>
            </db:BUSINESS_KEY_VALUE>
            <db:HOST>
               <xsl:text disable-output-escaping="no"/>
            </db:HOST>
            <db:PORT>
               <xsl:text disable-output-escaping="no"/>
            </db:PORT>
         </db:P_IN_LOG_RECORD>
      </db:InputParameters>
   </xsl:template>
</xsl:stylesheet>