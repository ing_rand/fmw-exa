<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:ircifr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.CreateIfaceRecord" 
xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction"
xmlns:irgifr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetIfaceRecord" 
xmlns:iruif="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdateIfaceStatus"
xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" 
xmlns:ns0="http://xmlns.irco.com/CustCreateReq"
xmlns:iruprec="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdatePhaseRec" 
xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" 
xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions"
xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" 
xmlns:oracle-xsl-mapper="http://www.oracle.com/xsl/mapper/schemas" 
xmlns:getasyncp="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetAsyncParent" 
xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue"
xmlns:oraxsl="http://www.oracle.com/XSL/Transform/java"
xmlns:tns="http://xmlns.oracle.com/Enterprise/Tools/schemas/M843613.V1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:iruperr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdateErrorRec" xmlns:irgsvurl="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetServiceURL" exclude-result-prefixes=" xsd oracle-xsl-mapper xsi xsl ns0 tns ircifr mhdr irgifr iruif oraext iruprec xp20 xref socket getasyncp dvm oraxsl iruperr irgsvurl"
                xmlns:plnk="http://schemas.xmlsoap.org/ws/2003/05/partner-link/"
                xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:ns2="http://xmlns.irco.com/ContactCreateResp"
                xmlns:client="http://xmlns.irco.com/PSCRMCustomerCreateReqABCSImpl"
                xmlns:M136072.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/M136072.V1"
                xmlns:M918200.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/M918200.V1"
                xmlns:wsp="http://schemas.xmlsoap.org/ws/2002/12/policy"
                xmlns:M930133.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/M930133.V1"
                xmlns:M171321.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/M171321.V1"
                xmlns:M660197.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/M660197.V1"
                xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/"
                xmlns:M1008352.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/M1008352.V1"
                xmlns:M851639.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/M851639.V1"
                xmlns:M140070.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/M140070.V1"
                xmlns:M207760.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/M207760.V1"
                xmlns:ns1="http://xmlns.oracle.com/Enterprise/Tools/services/CI_TC_COMPANY_CI.1"
                xmlns:M143702.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/M143702.V1"
                xmlns:M676524.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/M676524.V1"
                xmlns:M224393.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/M224393.V1"
                xmlns:M416262.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/M416262.V1"
                xmlns:M1046225.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/M1046225.V1"
                xmlns:ns4="http://schemas.oracle.com/bpel/extension"
                xmlns:ns3="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
                xmlns:ns5="http://xmlns.irco.com/CustCreateResp"
                xmlns:M1074607.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/M1074607.V1"
                xmlns:M383913.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/M383913.V1"
                xmlns:M569069.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/M569069.V1"
                xmlns:M487248.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/M487248.V1"
                xmlns:M818527.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/M818527.V1"
                xmlns:M766126.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/M766126.V1"
                xmlns:M999684.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/M999684.V1"
                xmlns:M676371.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/M676371.V1"
                xmlns:M884477.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/M884477.V1"
                xmlns:M289153.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/M289153.V1"
                xmlns:M638590.V1="http://xmlns.oracle.com/Enterprise/Tools/schemas/M638590.V1">
   <oracle-xsl-mapper:schema>
      <!--SPECIFICATION OF MAP SOURCES AND TARGETS, DO NOT MODIFY.-->
      <oracle-xsl-mapper:mapSources>
         <oracle-xsl-mapper:source type="WSDL">
            <oracle-xsl-mapper:schema location="../WSDLs/PSCRMCustomerCreateReqABCSImpl.wsdl"/>
            <oracle-xsl-mapper:rootElement name="CustomerCreateRequest" namespace="http://xmlns.irco.com/CustCreateReq"/>
         </oracle-xsl-mapper:source>
      </oracle-xsl-mapper:mapSources>
      <oracle-xsl-mapper:mapTargets>
         <oracle-xsl-mapper:target type="WSDL">
            <oracle-xsl-mapper:schema location="oramds:/apps/irsmart/EnterpriseServiceLibrary/CRM_PS/wsdl/CI_TC_COMPANY_CI.1.wsdl"/>
            <oracle-xsl-mapper:rootElement name="Create__CompIntfc__TC_COMPANY_CI" namespace="http://xmlns.oracle.com/Enterprise/Tools/schemas/M843613.V1"/>
         </oracle-xsl-mapper:target>
      </oracle-xsl-mapper:mapTargets>
      <!--GENERATED BY ORACLE XSL MAPPER 12.2.1.0.0(XSLT Build 151013.0700.0085) AT [THU MAR 01 14:58:50 EST 2018].-->
   </oracle-xsl-mapper:schema>
   <!--User Editing allowed BELOW this line - DO NOT DELETE THIS LINE-->
   <xsl:template match="/">
      <tns:Create__CompIntfc__TC_COMPANY_CI>
         <tns:SETID>
            <xsl:value-of select="/ns0:CustomerCreateRequest/ns0:ExtensibleAttributes/ns0:Attribute[ns0:Name='SETID']/ns0:Value"/>
         </tns:SETID>
         <tns:CUSTOMER_TYPE>
            <xsl:value-of select="/ns0:CustomerCreateRequest/ns0:CustomerType"/>
         </tns:CUSTOMER_TYPE>
         <tns:INDUSTRY_ID>
            <xsl:value-of select="/ns0:CustomerCreateRequest/ns0:IndustryID"/>
         </tns:INDUSTRY_ID>
         <tns:TC_FUNC_CODE_ID>
            <xsl:value-of select="/ns0:CustomerCreateRequest/ns0:ExtensibleAttributes/ns0:Attribute[ns0:Name='TC_FUNC_CODE_ID']/ns0:Value"/>
         </tns:TC_FUNC_CODE_ID>
         <tns:BO_NAME_CI_VW>
            <tns:NAME_TYPE>
               <xsl:value-of select="/ns0:CustomerCreateRequest/ns0:ExtensibleAttributes/ns0:Attribute[ns0:Name='NAME_TYPE']/ns0:Value"/>
            </tns:NAME_TYPE>
            <tns:PRIMARY_IND>
               <xsl:value-of select="/ns0:CustomerCreateRequest/ns0:ExtensibleAttributes/ns0:Attribute[ns0:Name='PRIMARY_IND']/ns0:Value"/>
            </tns:PRIMARY_IND>
            <tns:BO_NAME>
               <xsl:value-of select="/ns0:CustomerCreateRequest/ns0:CustomerName"/>
            </tns:BO_NAME>
         </tns:BO_NAME_CI_VW>
         <tns:BC_CI_VW>
            <tns:SEGMENT>
               <xsl:value-of select="/ns0:CustomerCreateRequest/ns0:AccountTier"/>
            </tns:SEGMENT>
         </tns:BC_CI_VW>
         <tns:BO_CI_ADDR_VW>
            <tns:BO_CM_START_DT>
               <xsl:value-of select="xp20:current-date ( )"/>
            </tns:BO_CM_START_DT>
            <tns:COUNTRY>
               <xsl:value-of select="/ns0:CustomerCreateRequest/ns0:Country"/>
            </tns:COUNTRY>
            <tns:ADDRESS1>
               <xsl:value-of select="/ns0:CustomerCreateRequest/ns0:Address1"/>
            </tns:ADDRESS1>
            <tns:ADDRESS2>
               <xsl:value-of select="/ns0:CustomerCreateRequest/ns0:Address2"/>
            </tns:ADDRESS2>
            <tns:CITY>
               <xsl:value-of select="/ns0:CustomerCreateRequest/ns0:City"/>
            </tns:CITY>
            <tns:STATE>
               <xsl:value-of select="/ns0:CustomerCreateRequest/ns0:State"/>
            </tns:STATE>
            <tns:POSTAL>
               <xsl:value-of select="/ns0:CustomerCreateRequest/ns0:PostalCode"/>
            </tns:POSTAL>
            <tns:ROW_ADDED_DTTM>
               <xsl:value-of select="xp20:current-date ( )"/>
            </tns:ROW_ADDED_DTTM>
            <tns:ROW_ADDED_OPRID>
               <xsl:value-of select="/ns0:CustomerCreateRequest/ns0:CreatedBy"/>
            </tns:ROW_ADDED_OPRID>
         </tns:BO_CI_ADDR_VW>
         <tns:TC_RD_CUST_REGN>
            <tns:REGION_ID>
               <xsl:value-of select="/ns0:CustomerCreateRequest/ns0:AccountOffice"/>
            </tns:REGION_ID>
         </tns:TC_RD_CUST_REGN>
      </tns:Create__CompIntfc__TC_COMPANY_CI>
   </xsl:template>
</xsl:stylesheet>
