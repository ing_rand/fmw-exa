<?xml version = '1.0' encoding = 'UTF-8'?>
<xsl:stylesheet version="1.0" xmlns:bpws="http://schemas.xmlsoap.org/ws/2003/03/business-process/" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:hwf="http://xmlns.oracle.com/bpel/workflow/xpath" xmlns:ns1="http://xmlns.irco.com/EnterpriseObjects/Core/Common/V1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:med="http://schemas.oracle.com/mediator/xpath" xmlns:ids="http://xmlns.oracle.com/bpel/services/IdentityService/xpath" xmlns:bpm="http://xmlns.oracle.com/bpmn20/extensions" xmlns:xdk="http://schemas.oracle.com/bpel/extension/xpath/function/xdk" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" xmlns:client="http://xmlns.irco.com/XxintCreateOutboundEventV1EsEbiz/IRFaultHandler/IRFaultEmailNotification" xmlns:plnk="http://schemas.xmlsoap.org/ws/2003/05/partner-link/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:ora="http://schemas.oracle.com/xpath/extension" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:ldap="http://schemas.oracle.com/xpath/extension/ldap" exclude-result-prefixes="xsi xsl ns1 client plnk xsd wsdl bpws xp20 mhdr bpel oraext dvm hwf med ids bpm xdk xref ora socket ldap">
   <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
   <xsl:template match="/">
      <html>
         <body>
            <b>ERROR SUMMARY:</b>
            <xsl:value-of select="/ns1:IRFault/ns1:FaultFacts/ns1:Summary"/>
            <br/>
            <table border="1">
               <colgroup span="1" width="150">
                  <colgroup span="1" width="1000">
                     <tr valign="top">
                        <td>
                           <b>INTERFACE NAME:</b>
                        </td>
                        <td>
                           <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:CompositeName"/>
                        </td>
                     </tr>
                     <tr valign="top">
                        <td>
                           <b>COMPOSITE INSTANCE ID:</b>
                        </td>
                        <td>
                           <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:CompositeInstanceID"/>
                        </td>
                     </tr>
                     <tr valign="top">
                        <td>
                           <b>SERVICE NAME:</b>
                        </td>
                        <td>
                           <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:CompositeName"/>
                        </td>
                     </tr>
                     <tr valign="top">
                        <td>
                           <b>DATE:</b>
                        </td>
                        <td>
                           <xsl:value-of select="/ns1:IRFault/ns1:FaultReportedDateTime"/>
                        </td>
                     </tr>
                     <tr valign="top">
                        <td>
                           <b>ERROR SHORT DESCRIPTION:</b>
                        </td>
                        <td>
                           <xsl:value-of select="/ns1:IRFault/ns1:FaultFacts/ns1:ShortDescription"/>
                        </td>
                     </tr>
                     <tr valign="top">
                        <td>
                           <b>ERROR LONG DESCRIPTION:</b>
                        </td>
                        <td>
                           <xsl:value-of select="/ns1:IRFault/ns1:FaultFacts/ns1:LongDescription"/>
                        </td>
                     </tr>
                     <tr valign="top">
                        <td>
                           <b>NOTE:</b>
                        </td>
                        <td>
                           <xsl:value-of select="/ns1:IRFault/ns1:FaultFacts/ns1:Note"/>
                        </td>
                     </tr>
                     <tr valign="top">
                        <td>
                           <b>TRACE:</b>
                        </td>
                        <td>
                           <xsl:value-of select="/ns1:IRFault/ns1:FaultFacts/ns1:Trace"/>
                        </td>
                     </tr>
                  </colgroup>
               </colgroup>
            </table>
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>