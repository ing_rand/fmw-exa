<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version="1.0"
                xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator"
                xmlns:oracle-xsl-mapper="http://www.oracle.com/xsl/mapper/schemas"
                xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue"
                xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction"
                xmlns:oraxsl="http://www.oracle.com/XSL/Transform/java"
                xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions"
                xmlns:ns0="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
                exclude-result-prefixes="oracle-xsl-mapper xsi xsd xsl ns0 socket dvm mhdr oraxsl oraext xp20 xref"
                xmlns:plnk="http://schemas.xmlsoap.org/ws/2003/05/partner-link/"
                xmlns:ns2="http://xmlns.irco.com/EnterpriseObjects/Core/Common/V1"
                xmlns:client="http://xmlns.irco.com/P21PricingandAvailabilityProvABCSImpl/P21PricingandAvailbilityService"
                xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"
                xmlns:ns3="http://xmlns.irco.com/GetPricingandAvailabilityRequest"
                xmlns:ns4="http://xmlns.irco.com/GetPricingResponse"
                xmlns:ns1="http://xmlns.irco.com/GetItemPrice_request"
                xmlns:ns5="http://xmlns.irco.com/P21PricingandAvailabilityProvImpl/P21PricingandAvailbilityService"
                xmlns:ns6="http://schemas.oracle.com/bpel/extension"
                xmlns:irgsvurl="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetServiceURL"
                xmlns:getasyncp="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetAsyncParent"
                xmlns:iruprec="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdatePhaseRec"
                xmlns:iruperr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdateErrorRec"
                xmlns:ircifr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.CreateIfaceRecord"
                xmlns:irgifr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetIfaceRecord"
                xmlns:iruif="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdateIfaceStatus">
  <oracle-xsl-mapper:schema>
    <oracle-xsl-mapper:mapSources>
      <oracle-xsl-mapper:source type="WSDL">
        <oracle-xsl-mapper:schema location="../WSDLs/P21PricingandAvailbilityService.wsdl"/>
        <oracle-xsl-mapper:rootElement name="Security"
                                       namespace="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"/>
      </oracle-xsl-mapper:source>
    </oracle-xsl-mapper:mapSources>
    <oracle-xsl-mapper:mapTargets>
      <oracle-xsl-mapper:target type="WSDL">
        <oracle-xsl-mapper:schema location="../WSDLs/P21PricingandAvailbilityService.wsdl"/>
        <oracle-xsl-mapper:rootElement name="G_INPUT" namespace=""/>
      </oracle-xsl-mapper:target>
    </oracle-xsl-mapper:mapTargets>
  </oracle-xsl-mapper:schema>
  <xsl:template match="/">
    <G_INPUT>
      <xsl:for-each select="/ns0:Security/ns0:UsernameToken/ns0:Username">
        <PARAMETER>
          <NAME>p_parent_service_name</NAME>
          <VALUE>P21PricingandAvailabilityProvABCSImpl</VALUE>
        </PARAMETER>
        <PARAMETER>
          <NAME>p_child_service_name</NAME>
          <VALUE>P21_Pricing_Service_ep</VALUE>
        </PARAMETER>
        <PARAMETER>
          <NAME>p_account_name</NAME>
          <VALUE>
            <xsl:value-of select="."/>
          </VALUE>
        </PARAMETER>
      </xsl:for-each>
    </G_INPUT>
  </xsl:template>
</xsl:stylesheet>
