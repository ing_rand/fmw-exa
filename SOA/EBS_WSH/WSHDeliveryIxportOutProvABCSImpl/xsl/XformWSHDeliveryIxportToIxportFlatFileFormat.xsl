<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version="1.0" xmlns:bpws="http://schemas.xmlsoap.org/ws/2003/03/business-process/" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns1="http://model/events/schema/XxsgIfaceDetailEO" xmlns:ns3="http://xmlns.irco.com/EnterpriseObjects/Core/Common/V1" xmlns:bpm="http://xmlns.oracle.com/bpmn20/extensions" xmlns:irbpelx="http://xmlns.irco.com/BPEL/ir/foundation/soa/extensions/xpath" xmlns:plnk="http://schemas.xmlsoap.org/ws/2003/05/partner-link/" xmlns:client="http://xmlns.irco.com/WSHDeliveryIxportOutProvABCSImpl" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:ora="http://schemas.oracle.com/xpath/extension" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:tns="http://xmlns.oracle.com/pcbpel/adapter/ftp/XXWSH1989/WSHDeliveryIxportOutProvABCSImpl/FTPService" xmlns:ns0="http://xmlns.oracle.com/pcbpel/event/PhaseFeed" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:ns2="http://xmlns.irco.com/WSHDeliveryOutProvABCSImpl" xmlns:hwf="http://xmlns.oracle.com/bpel/workflow/xpath" xmlns:jca="http://xmlns.oracle.com/pcbpel/wsdl/jca/" xmlns:med="http://schemas.oracle.com/mediator/xpath" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ids="http://xmlns.oracle.com/bpel/services/IdentityService/xpath" xmlns:xdk="http://schemas.oracle.com/bpel/extension/xpath/function/xdk" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" xmlns:imp1="http://xmlns.oracle.com/IXPORT_FileFormat" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:ns4="http://xmlns.oracle.com/pcbpel/adapter/db/top/UpdateSOAInterfaceRecStatusDBService" xmlns:ns5="http://xmlns.oracle.com/pcbpel/adapter/db/IRSOA/XXSG_UTL_IFACE_V1_PKG/UPSERT_IFACE_ERROR_RECORD/" xmlns:ldap="http://schemas.oracle.com/xpath/extension/ldap" exclude-result-prefixes="xsi xsl ns1 ns3 plnk client wsdl ns0 ns2 xsd ns4 ns5 tns jca imp1 bpws xp20 bpel bpm irbpelx ora socket mhdr oraext dvm hwf med ids xdk xref ldap">
   <xsl:template match="/">
      <imp1:Root>
         <imp1:HEADER>
            <imp1:LAST_UPDATE_DATE>
               <xsl:text disable-output-escaping="no">LAST_UPDATE_DATE</xsl:text>
            </imp1:LAST_UPDATE_DATE>
            <imp1:LAST_UPDATED_BY>
               <xsl:text disable-output-escaping="no">LAST_UPDATED_BY</xsl:text>
            </imp1:LAST_UPDATED_BY>
            <imp1:CREATION_DATE>
               <xsl:text disable-output-escaping="no">CREATION_DATE</xsl:text>
            </imp1:CREATION_DATE>
            <imp1:CREATED_BY>
               <xsl:text disable-output-escaping="no">CREATED_BY</xsl:text>
            </imp1:CREATED_BY>
            <imp1:LAST_UPDATE_LOGIN>
               <xsl:text disable-output-escaping="no">LAST_UPDATE_LOGIN</xsl:text>
            </imp1:LAST_UPDATE_LOGIN>
            <imp1:TRAILER_NUMBER>
               <xsl:text disable-output-escaping="no">TRAILER_NUMBER</xsl:text>
            </imp1:TRAILER_NUMBER>
            <imp1:CONTROL_NUMBER>
               <xsl:text disable-output-escaping="no">CONTROL_NUMBER</xsl:text>
            </imp1:CONTROL_NUMBER>
            <imp1:ORGANIZATION_CODE>
               <xsl:text disable-output-escaping="no">ORGANIZATION_CODE</xsl:text>
            </imp1:ORGANIZATION_CODE>
            <imp1:ORGANIZATION_ID>
               <xsl:text disable-output-escaping="no">ORGANIZATION_ID</xsl:text>
            </imp1:ORGANIZATION_ID>
            <imp1:ITEM_NUMBER>
               <xsl:text disable-output-escaping="no">ITEM_NUMBER</xsl:text>
            </imp1:ITEM_NUMBER>
            <imp1:INVENTORY_ITEM_ID>
               <xsl:text disable-output-escaping="no">INVENTORY_ITEM_ID</xsl:text>
            </imp1:INVENTORY_ITEM_ID>
            <imp1:ITEM_DESCRIPTION>
               <xsl:text disable-output-escaping="no">ITEM_DESCRIPTION</xsl:text>
            </imp1:ITEM_DESCRIPTION>
            <imp1:WAYBILL_NUMBER>
               <xsl:text disable-output-escaping="no">WAYBILL_NUMBER</xsl:text>
            </imp1:WAYBILL_NUMBER>
            <imp1:ORDER_NUMBER>
               <xsl:text disable-output-escaping="no">ORDER_NUMBER</xsl:text>
            </imp1:ORDER_NUMBER>
            <imp1:ORDER_LINE_NUMBER>
               <xsl:text disable-output-escaping="no">ORDER_LINE_NUMBER</xsl:text>
            </imp1:ORDER_LINE_NUMBER>
            <imp1:SHIPPED_QUANTITY>
               <xsl:text disable-output-escaping="no">SHIPPED_QUANTITY</xsl:text>
            </imp1:SHIPPED_QUANTITY>
            <imp1:CARTON_QUANTITY>
               <xsl:text disable-output-escaping="no">CARTON_QUANTITY</xsl:text>
            </imp1:CARTON_QUANTITY>
            <imp1:PALLET_QUANTITY>
               <xsl:text disable-output-escaping="no">PALLET_QUANTITY</xsl:text>
            </imp1:PALLET_QUANTITY>
            <imp1:CUSTOMER_PO_NUMBER>
               <xsl:text disable-output-escaping="no">CUSTOMER_PO_NUMBER</xsl:text>
            </imp1:CUSTOMER_PO_NUMBER>
            <imp1:PICKSLIP_NUMBER>
               <xsl:text disable-output-escaping="no">PICKSLIP_NUMBER</xsl:text>
            </imp1:PICKSLIP_NUMBER>
            <imp1:PALLET_ID>
               <xsl:text disable-output-escaping="no">PALLET_ID</xsl:text>
            </imp1:PALLET_ID>
            <imp1:SHIPMENT_DATE>
               <xsl:text disable-output-escaping="no">SHIPMENT_DATE</xsl:text>
            </imp1:SHIPMENT_DATE>
            <imp1:NET_PRICE>
               <xsl:text disable-output-escaping="no">NET_PRICE</xsl:text>
            </imp1:NET_PRICE>
            <imp1:EXTENDED_PRICE>
               <xsl:text disable-output-escaping="no">EXTENDED_PRICE</xsl:text>
            </imp1:EXTENDED_PRICE>
            <imp1:US_LICENSE_PLATE>
               <xsl:text disable-output-escaping="no">US_LICENSE_PLATE</xsl:text>
            </imp1:US_LICENSE_PLATE>
            <imp1:SEAL_NBR1>
               <xsl:text disable-output-escaping="no">SEAL_NBR</xsl:text>
            </imp1:SEAL_NBR1>
            <imp1:MEX_TRANS_CODE1>
               <xsl:text disable-output-escaping="no">MEX_TRANS_CODE</xsl:text>
            </imp1:MEX_TRANS_CODE1>
            <imp1:MEX_LICENSE_PLATE>
               <xsl:text disable-output-escaping="no">MEX_LICENSE_PLATE</xsl:text>
            </imp1:MEX_LICENSE_PLATE>
            <imp1:BATCH_NUMBER>
               <xsl:text disable-output-escaping="no">BATCH_NUMBER</xsl:text>
            </imp1:BATCH_NUMBER>
            <imp1:PART_WEIGHT>
               <xsl:text disable-output-escaping="no">PART_WEIGHT</xsl:text>
            </imp1:PART_WEIGHT>
            <imp1:SHIP_TO_CUSTOMER_NAME>
               <xsl:text disable-output-escaping="no">SHIP_TO_CUSTOMER_NAME</xsl:text>
            </imp1:SHIP_TO_CUSTOMER_NAME>
            <imp1:SHIP_TO_ADDRESS_1>
               <xsl:text disable-output-escaping="no">SHIP_TO_ADDRESS_1</xsl:text>
            </imp1:SHIP_TO_ADDRESS_1>
            <imp1:SHIP_TO_ADDRESS_2>
               <xsl:text disable-output-escaping="no">SHIP_TO_ADDRESS_2</xsl:text>
            </imp1:SHIP_TO_ADDRESS_2>
            <imp1:SHIP_TO_ADDRESS_3>
               <xsl:text disable-output-escaping="no">SHIP_TO_ADDRESS_3</xsl:text>
            </imp1:SHIP_TO_ADDRESS_3>
            <imp1:CITY>
               <xsl:text disable-output-escaping="no">CITY</xsl:text>
            </imp1:CITY>
            <imp1:STATE>
               <xsl:text disable-output-escaping="no">STATE</xsl:text>
            </imp1:STATE>
            <imp1:ZIP>
               <xsl:text disable-output-escaping="no">ZIP</xsl:text>
            </imp1:ZIP>
            <imp1:COUNTRY>
               <xsl:text disable-output-escaping="no">COUNTRY</xsl:text>
            </imp1:COUNTRY>
            <imp1:MASTER_CONTROL_NUMBER>
               <xsl:text disable-output-escaping="no">MASTER_CONTROL_NUMBER</xsl:text>
            </imp1:MASTER_CONTROL_NUMBER>
            <imp1:SEAL_NBR2>
               <xsl:text disable-output-escaping="no">SEAL_NBR2</xsl:text>
            </imp1:SEAL_NBR2>
            <imp1:MEX_TRANS_CODE2>
               <xsl:text disable-output-escaping="no">MX_TRANS_CODE</xsl:text>
            </imp1:MEX_TRANS_CODE2>
            <imp1:US_TRAILER_STATE>
               <xsl:text disable-output-escaping="no">US_TRAILER_STATE</xsl:text>
            </imp1:US_TRAILER_STATE>
             <!-- Start Changes for CR CR23718 Added For each for multiple WSH_DELIVERY_BOX -->
            <imp1:SERIAL_NUMBER_TEXT>
               <xsl:text disable-output-escaping="no">SERIAL_NUMBER_TEXT</xsl:text>
            </imp1:SERIAL_NUMBER_TEXT>
            <imp1:PALLET_GROSS_WEIGHT>
               <xsl:text disable-output-escaping="no">PALLET_GROSS_WEIGHT</xsl:text>
            </imp1:PALLET_GROSS_WEIGHT>
            <!--<imp1:LPN_ID>
               <xsl:text disable-output-escaping="no">LPN_ID</xsl:text>
            </imp1:LPN_ID>
            <imp1:PALLET_ID>
               <xsl:text disable-output-escaping="no">PALLET_ID</xsl:text>
            </imp1:PALLET_ID>-->
            <imp1:TRAILER_GROSS_WEIGHT>
               <xsl:text disable-output-escaping="no">TRAILER_GROSS_WEIGHT</xsl:text>
            </imp1:TRAILER_GROSS_WEIGHT>
            <!-- End of change for CR23718 -->  
         </imp1:HEADER>
         <imp1:DTL>
                  <!-- Start Changes for CR CR23718 Added For each for multiple WSH_DELIVERY_BOX -->
         <xsl:for-each select="/ns2:IR_EBS_DELIVERY_SYNC/G_WSH_DELIVERY_HDR/G_WSH_DELIVERY_HDR_ROW/WSH_DELIVERY_DETAIL_HDR/WSH_DELIVERY_BOX">
       <!--  <imp1:WSH_DELIVERY_BOX>-->
         <xsl:for-each select="./WSH_DELIVERY_BOX_LINE/WSH_DELIVERY_BOX_LINE_ROW">
               <imp1:WSH_DELIVERY_BOX_LINE_ROW>
                  <imp1:LAST_UPDATE_DATE>
                     <xsl:value-of select="xp20:format-dateTime(./LAST_UPDATE_DATE,'[M01]-[D01]-[Y0001]')"/>
                  </imp1:LAST_UPDATE_DATE>
                  <imp1:LAST_UPDATED_BY>
                     <xsl:value-of select="./LAST_UPDATED_BY"/>
                  </imp1:LAST_UPDATED_BY>
                  <imp1:CREATION_DATE>
                     <xsl:value-of select="xp20:format-dateTime(./CREATION_DATE,'[M01]-[D01]-[Y0001]')"/>
                  </imp1:CREATION_DATE>
                  <imp1:CREATED_BY>
                     <xsl:value-of select="./CREATED_BY"/>
                  </imp1:CREATED_BY>
                  <imp1:LAST_UPDATE_LOGIN>
                     <xsl:value-of select="./LAST_UPDATE_LOGIN"/>
                  </imp1:LAST_UPDATE_LOGIN>
                  <imp1:VEHICLE_NUMBER>
                     <xsl:value-of select="../../../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/VEHICLE_NUMBER"/>
                  </imp1:VEHICLE_NUMBER>
                  <imp1:TRIP_ID1>
                     <xsl:value-of select="../../../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/TRIP_ID"/>
                  </imp1:TRIP_ID1>
                  <imp1:DD_ORG_CODE>
                     <xsl:value-of select="./DD_ORG_CODE"/>
                  </imp1:DD_ORG_CODE>
                  <imp1:ORGANIZATION_ID>
                     <xsl:value-of select="./ORGANIZATION_ID"/>
                  </imp1:ORGANIZATION_ID>
                  <imp1:ITEM_NUMBER>
                     <xsl:value-of select="./ITEM_NUMBER"/>
                  </imp1:ITEM_NUMBER>
                  <imp1:INVENTORY_ITEM_ID>
                     <xsl:value-of select="./INVENTORY_ITEM_ID"/>
                  </imp1:INVENTORY_ITEM_ID>
                  <imp1:ITEM_DESCRIPTION>
                     <xsl:value-of select="./ITEM_DESCRIPTION"/>
                  </imp1:ITEM_DESCRIPTION>
                  <imp1:WAYBILL>
                     <xsl:value-of select="../../../../WAYBILL"/>
                  </imp1:WAYBILL>
                  <imp1:SOURCE_HEADER_NUMBER>
                     <xsl:value-of select="./SOURCE_HEADER_NUMBER"/>
                  </imp1:SOURCE_HEADER_NUMBER>
                  <imp1:SOURCE_LINE_NUMBER>
                     <xsl:value-of select="./SOURCE_LINE_NUMBER"/>
                  </imp1:SOURCE_LINE_NUMBER>
                  <imp1:SHIPPED_QUANTITY>
                     <xsl:value-of select="./SHIPPED_QUANTITY"/>
                  </imp1:SHIPPED_QUANTITY>
                  <imp1:CARTON_QUANTITY>
                     <xsl:value-of select="../../../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/CARTON_QUANTITY"/>
                  </imp1:CARTON_QUANTITY>
                  <imp1:PALLET_QUANTITY>
                     <xsl:value-of select="../../../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/PALLET_QUANTITY"/>
                  </imp1:PALLET_QUANTITY>
                  <imp1:CUST_PO_NUMBER>
                     <xsl:value-of select="./CUST_PO_NUMBER"/>
                  </imp1:CUST_PO_NUMBER>
                  <imp1:PACKING_SLIP>
                     <xsl:value-of select="../../../../PACKING_SLIP"/>
                  </imp1:PACKING_SLIP>
                  <imp1:PALLET_ID>
                     <xsl:value-of select="PALLET_ID"/>
                  </imp1:PALLET_ID>
                  <imp1:SHIPPED_DATE>
                     <xsl:value-of select="xp20:format-dateTime(./SHIPPED_DATE,'[M01]-[D01]-[Y0001]')"/>
                  </imp1:SHIPPED_DATE>
                  <imp1:NET_PRICE>
                     <xsl:value-of select="./NET_PRICE"/>
                  </imp1:NET_PRICE>
                  <imp1:EXTENDED_PRICE>
                     <xsl:value-of select="./EXTENDED_PRICE"/>
                  </imp1:EXTENDED_PRICE>
                  <imp1:DESTINATION_LICENSE_PLATE>
                     <xsl:value-of select="../../../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/DESTINATION_LICENSE_PLATE"/>
                  </imp1:DESTINATION_LICENSE_PLATE>
                  <imp1:SEAL_CODE1>
                     <xsl:choose>
                        <xsl:when test="../../../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/SEAL_CODE !=''">
                           <xsl:value-of select="../../../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/SEAL_CODE"/>
                        </xsl:when>
                        <xsl:otherwise>
                           <xsl:value-of select="./SEAL_CODE"/>
                        </xsl:otherwise>
                     </xsl:choose>
                  </imp1:SEAL_CODE1>
                  <imp1:SOURCE_TRANS_CODE>
                     <xsl:value-of select="../../../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/SOURCE_TRANS_CODE"/>
                  </imp1:SOURCE_TRANS_CODE>
                  <imp1:SOURCE_LICENSE_PLATE>
                     <xsl:value-of select="../../../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/SOURCE_LICENSE_PLATE"/>
                  </imp1:SOURCE_LICENSE_PLATE>
                  <imp1:BATCH_NUMBER>
                     <xsl:value-of select="../../../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/BATCH_NUMBER"/>
                  </imp1:BATCH_NUMBER>
                  <imp1:NET_WEIGHT>
                     <xsl:value-of select="./NET_WEIGHT"/>
                  </imp1:NET_WEIGHT>
                  <imp1:CUST_NAME>
                     <xsl:value-of select="./CUST_NAME"/>
                  </imp1:CUST_NAME>
                  <imp1:ADDRESS1>
                     <xsl:value-of select="./SHIP_TO_ADDRESS/ADDRESS1"/>
                  </imp1:ADDRESS1>
                  <imp1:ADDRESS2>
                     <xsl:value-of select="./SHIP_TO_ADDRESS/ADDRESS2"/>
                  </imp1:ADDRESS2>
                  <imp1:ADDRESS3>
                     <xsl:value-of select="./SHIP_TO_ADDRESS/ADDRESS3"/>
                  </imp1:ADDRESS3>
                  <imp1:CITY>
                     <xsl:value-of select="./SHIP_TO_ADDRESS/CITY"/>
                  </imp1:CITY>
                  <imp1:STATE>
                     <xsl:value-of select="./SHIP_TO_ADDRESS/STATE"/>
                  </imp1:STATE>
                  <imp1:POSTAL_CODE>
                     <xsl:value-of select="./SHIP_TO_ADDRESS/POSTAL_CODE"/>
                  </imp1:POSTAL_CODE>
                  <imp1:COUNTRY>
                     <xsl:value-of select="./SHIP_TO_ADDRESS/COUNTRY"/>
                  </imp1:COUNTRY>
                  <imp1:TRIP_ID2>
                     <xsl:value-of select="../../../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/TRIP_ID"/>
                  </imp1:TRIP_ID2>
                  <imp1:SEAL_CODE2>
                     <xsl:choose>
                        <xsl:when test="../../../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/SEAL_CODE != ''">
                           <xsl:value-of select="../../../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/SEAL_CODE"/>
                        </xsl:when>
                        <xsl:otherwise>
                           <xsl:value-of select="SEAL_CODE"/>
                        </xsl:otherwise>
                     </xsl:choose>
                  </imp1:SEAL_CODE2>
                  <imp1:SOURCE_TRANS_CODE1>
                     <xsl:value-of select="../../../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/SOURCE_TRANS_CODE1"/>
                  </imp1:SOURCE_TRANS_CODE1>
                  <imp1:DESTINATION_TRAILER_STATE>
                     <xsl:value-of select="../../../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/DESTINATION_TRAILER_STATE"/>
                  </imp1:DESTINATION_TRAILER_STATE>
                     <!-- Start Changes for CR CR23718 -->
                <imp1:SERIAL_NUMBER_TEXT>
                    <xsl:value-of select="./SERIAL_NUMBER_TEXT"/>
                </imp1:SERIAL_NUMBER_TEXT>
                <!--DeliveryBox Mappings-->
                <imp1:PALLET_GROSS_WEIGHT>
               <!-- <xsl:value-of select="concat((../../GROSS_WEIGHT),(../../WEIGHT_UOM_CODE))"/> -->
                <xsl:value-of select="../../GROSS_WEIGHT"/>
            </imp1:PALLET_GROSS_WEIGHT>
             <!--<imp1:PALLET_ID>
                <xsl:value-of select="../../PALLET_ID"/>
            </imp1:PALLET_ID>-->

        <imp1:TRAILER_GROSS_WEIGHT> 
            <!--<xsl:value-of select="concat((/ns2:IR_EBS_DELIVERY_SYNC/G_WSH_DELIVERY_HDR/G_WSH_DELIVERY_HDR_ROW/WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/WSH_TRIP_STOPS_HDR/WSH_TRIP_STOPS_HDR_ROW/DEPARTURE_GROSS_WEIGHT),
            (/ns2:IR_EBS_DELIVERY_SYNC/G_WSH_DELIVERY_HDR/G_WSH_DELIVERY_HDR_ROW/WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/WSH_TRIP_STOPS_HDR/WSH_TRIP_STOPS_HDR_ROW/WEIGHT_UOM_CODE))"/>-->
            <xsl:value-of select="/ns2:IR_EBS_DELIVERY_SYNC/G_WSH_DELIVERY_HDR/G_WSH_DELIVERY_HDR_ROW/WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/WSH_TRIP_STOPS_HDR/WSH_TRIP_STOPS_HDR_ROW/DEPARTURE_GROSS_WEIGHT"/>
        </imp1:TRAILER_GROSS_WEIGHT>
                  
               </imp1:WSH_DELIVERY_BOX_LINE_ROW>
               
            </xsl:for-each>
         <!--    Delivery Box Line
            
            </imp1:WSH_DELIVERY_BOX> -->
        </xsl:for-each>
            <!--<xsl:for-each select="/ns2:IR_EBS_DELIVERY_SYNC/G_WSH_DELIVERY_HDR/G_WSH_DELIVERY_HDR_ROW/WSH_DELIVERY_DETAIL_HDR/WSH_DELIVERY_BOX/WSH_DELIVERY_BOX_LINE/WSH_DELIVERY_BOX_LINE_ROW">-->
               <!-- End Changes for CR CR23718 -->
            <xsl:for-each select="/ns2:IR_EBS_DELIVERY_SYNC/G_WSH_DELIVERY_HDR/G_WSH_DELIVERY_HDR_ROW/WSH_DEL_INDIVIDUAL_LINES/WSH_DEL_INDIVIDUAL_LINE_ROW">
               <imp1:WSH_DEL_INDIVIDUAL_LINE_ROW>
                  <imp1:LAST_UPDATE_DATE>
                     <xsl:value-of select="xp20:format-dateTime(LAST_UPDATE_DATE,'[M01]-[D01]-[Y0001]')"/>
                  </imp1:LAST_UPDATE_DATE>
                  <imp1:LAST_UPDATED_BY>
                     <xsl:value-of select="LAST_UPDATED_BY"/>
                  </imp1:LAST_UPDATED_BY>
                  <imp1:CREATION_DATE>
                     <xsl:value-of select="xp20:format-dateTime(CREATION_DATE,'[M01]-[D01]-[Y0001]')"/>
                  </imp1:CREATION_DATE>
                  <imp1:CREATED_BY>
                     <xsl:value-of select="CREATED_BY"/>
                  </imp1:CREATED_BY>
                  <imp1:LAST_UPDATE_LOGIN>
                     <xsl:value-of select="LAST_UPDATE_LOGIN"/>
                  </imp1:LAST_UPDATE_LOGIN>
                  <imp1:VEHICLE_NUMBER>
                     <xsl:value-of select="../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/VEHICLE_NUMBER"/>
                  </imp1:VEHICLE_NUMBER>
                  <imp1:TRIP_ID1>
                     <xsl:value-of select="../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/TRIP_ID"/>
                  </imp1:TRIP_ID1>
                  <imp1:DD_ORG_CODE>
                     <xsl:value-of select="DD_ORG_CODE"/>
                  </imp1:DD_ORG_CODE>
                  <imp1:ORGANIZATION_ID>
                     <xsl:value-of select="ORGANIZATION_ID"/>
                  </imp1:ORGANIZATION_ID>
                  <imp1:ITEM_NUMBER>
                     <xsl:value-of select="ITEM_NUMBER"/>
                  </imp1:ITEM_NUMBER>
                  <imp1:INVENTORY_ITEM_ID>
                     <xsl:value-of select="INVENTORY_ITEM_ID"/>
                  </imp1:INVENTORY_ITEM_ID>
                  <imp1:ITEM_DESCRIPTION>
                     <xsl:value-of select="ITEM_DESCRIPTION"/>
                  </imp1:ITEM_DESCRIPTION>
                  <imp1:WAYBILL>
                     <xsl:value-of select="../../WAYBILL"/>
                  </imp1:WAYBILL>
                  <imp1:SOURCE_HEADER_NUMBER>
                     <xsl:value-of select="SOURCE_HEADER_NUMBER"/>
                  </imp1:SOURCE_HEADER_NUMBER>
                  <imp1:SOURCE_LINE_NUMBER>
                     <xsl:value-of select="SOURCE_LINE_NUMBER"/>
                  </imp1:SOURCE_LINE_NUMBER>
                  <imp1:SHIPPED_QUANTITY>
                     <xsl:value-of select="SHIPPED_QUANTITY"/>
                  </imp1:SHIPPED_QUANTITY>
                  <imp1:CARTON_QUANTITY>
                     <xsl:value-of select="../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/CARTON_QUANTITY"/>
                  </imp1:CARTON_QUANTITY>
                  <imp1:PALLET_QUANTITY>
                     <xsl:value-of select="../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/PALLET_QUANTITY"/>
                  </imp1:PALLET_QUANTITY>
                  <imp1:CUST_PO_NUMBER>
                     <xsl:value-of select="CUST_PO_NUMBER"/>
                  </imp1:CUST_PO_NUMBER>
                  <imp1:PACKING_SLIP>
                     <xsl:value-of select="../../PACKING_SLIP"/>
                  </imp1:PACKING_SLIP>
                  <imp1:PALLET_ID>
                     <xsl:value-of select="PALLET_ID"/>
                  </imp1:PALLET_ID>
                  <imp1:SHIPPED_DATE>
                     <xsl:value-of select="xp20:format-dateTime(SHIPPED_DATE,'[M01]-[D01]-[Y0001]')"/>
                  </imp1:SHIPPED_DATE>
                  <imp1:NET_PRICE>
                     <xsl:value-of select="NET_PRICE"/>
                  </imp1:NET_PRICE>
                  <imp1:EXTENDED_PRICE>
                     <xsl:value-of select="EXTENDED_PRICE"/>
                  </imp1:EXTENDED_PRICE>
                  <imp1:DESTINATION_LICENSE_PLATE>
                     <xsl:value-of select="../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/DESTINATION_LICENSE_PLATE"/>
                  </imp1:DESTINATION_LICENSE_PLATE>
                  <imp1:SEAL_CODE1>
                     <xsl:choose>
                        <xsl:when test="../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/SEAL_CODE !=''">
                           <xsl:value-of select="../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/SEAL_CODE"/>
                        </xsl:when>
                        <xsl:otherwise>
                           <xsl:value-of select="SEAL_CODE"/>
                        </xsl:otherwise>
                     </xsl:choose>
                  </imp1:SEAL_CODE1>
                  <imp1:SOURCE_TRANS_CODE>
                     <xsl:value-of select="../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/SOURCE_TRANS_CODE"/>
                  </imp1:SOURCE_TRANS_CODE>
                  <imp1:SOURCE_LICENSE_PLATE>
                     <xsl:value-of select="../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/SOURCE_LICENSE_PLATE"/>
                  </imp1:SOURCE_LICENSE_PLATE>
                  <imp1:BATCH_NUMBER>
                     <xsl:value-of select="../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/BATCH_NUMBER"/>
                  </imp1:BATCH_NUMBER>
                  <imp1:NET_WEIGHT>
                     <xsl:value-of select="NET_WEIGHT"/>
                  </imp1:NET_WEIGHT>
                  <imp1:CUST_NAME>
                     <xsl:value-of select="CUST_NAME"/>
                  </imp1:CUST_NAME>
                  <imp1:ADDRESS1>
                     <xsl:value-of select="SHIP_TO_ADDRESS/ADDRESS1"/>
                  </imp1:ADDRESS1>
                  <imp1:ADDRESS2>
                     <xsl:value-of select="SHIP_TO_ADDRESS/ADDRESS2"/>
                  </imp1:ADDRESS2>
                  <imp1:ADDRESS3>
                     <xsl:value-of select="SHIP_TO_ADDRESS/ADDRESS3"/>
                  </imp1:ADDRESS3>
                  <imp1:CITY>
                     <xsl:value-of select="SHIP_TO_ADDRESS/CITY"/>
                  </imp1:CITY>
                  <imp1:STATE>
                     <xsl:value-of select="SHIP_TO_ADDRESS/STATE"/>
                  </imp1:STATE>
                  <imp1:POSTAL_CODE>
                     <xsl:value-of select="SHIP_TO_ADDRESS/POSTAL_CODE"/>
                  </imp1:POSTAL_CODE>
                  <imp1:COUNTRY>
                     <xsl:value-of select="SHIP_TO_ADDRESS/COUNTRY"/>
                  </imp1:COUNTRY>
                  <imp1:TRIP_ID2>
                     <xsl:value-of select="../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/TRIP_ID"/>
                  </imp1:TRIP_ID2>
                  <imp1:SEAL_CODE2>
                     <xsl:choose>
                        <xsl:when test="../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/SEAL_CODE != ''">
                           <xsl:value-of select="../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/SEAL_CODE"/>
                        </xsl:when>
                        <xsl:otherwise>
                           <xsl:value-of select="SEAL_CODE"/>
                        </xsl:otherwise>
                     </xsl:choose>
                  </imp1:SEAL_CODE2>
                  <imp1:SOURCE_TRANS_CODE1>
                     <xsl:value-of select="../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/SOURCE_TRANS_CODE1"/>
                  </imp1:SOURCE_TRANS_CODE1>
                  <imp1:DESTINATION_TRAILER_STATE>
                     <xsl:value-of select="../../WSH_TRIPS_HDR/WSH_TRIPS_HDR_ROW/DESTINATION_TRAILER_STATE"/>
                  </imp1:DESTINATION_TRAILER_STATE>
                  
               </imp1:WSH_DEL_INDIVIDUAL_LINE_ROW>
            </xsl:for-each>
         </imp1:DTL>
      </imp1:Root>
   </xsl:template>
</xsl:stylesheet>
