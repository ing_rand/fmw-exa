<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version="1.0"
                xmlns:iriifp="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.InsertIfacePayload"
                xmlns:oraxsl="http://www.oracle.com/XSL/Transform/java"
                xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20"
                xmlns:ircifr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.CreateIfaceRecord"
                xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction"
                xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue"
                xmlns:oracle-xsl-mapper="http://www.oracle.com/xsl/mapper/schemas"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:irgifr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetIfaceRecord"
                xmlns:irgsvurl="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetServiceURL"
                xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator"
                xmlns:ns0="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
                exclude-result-prefixes="xsi oracle-xsl-mapper xsl xsd ns0 iriifp oraxsl xp20 ircifr mhdr oraext dvm irgifr irgsvurl xref socket">
  <oracle-xsl-mapper:schema>
    <oracle-xsl-mapper:mapSources>
      <oracle-xsl-mapper:source type="WSDL">
        <oracle-xsl-mapper:schema location="../DRFGetServiceURL.wsdl"/>
        <oracle-xsl-mapper:rootElement name="Security"
                                       namespace="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"/>
      </oracle-xsl-mapper:source>
      <oracle-xsl-mapper:source type="WSDL">
        <oracle-xsl-mapper:schema location="../DRFGetServiceURL.wsdl"/>
        <oracle-xsl-mapper:rootElement name="Server_Detail" namespace=""/>
        <oracle-xsl-mapper:param name="var_ServerDetail"/>
      </oracle-xsl-mapper:source>
    </oracle-xsl-mapper:mapSources>
    <oracle-xsl-mapper:mapTargets>
      <oracle-xsl-mapper:target type="WSDL">
        <oracle-xsl-mapper:schema location="../DRFGetServiceURL.wsdl"/>
        <oracle-xsl-mapper:rootElement name="G_INPUT" namespace=""/>
      </oracle-xsl-mapper:target>
    </oracle-xsl-mapper:mapTargets>
  </oracle-xsl-mapper:schema>
  <xsl:param name="var_ServerDetail"/>
  <xsl:template match="/">
    <G_INPUT>
      <xsl:for-each select="/ns0:Security/ns0:UsernameToken/ns0:Username">
        <PARAMETER>
          <NAME>
            <xsl:text disable-output-escaping="no">p_parent_service_name</xsl:text>
          </NAME>
          <VALUE>
            <xsl:text disable-output-escaping="no">IRSGAQSubscriberService</xsl:text>
          </VALUE>
        </PARAMETER>
        <PARAMETER>
          <NAME>
            <xsl:text disable-output-escaping="no">p_child_service_name</xsl:text>
          </NAME>
          <VALUE>
            <xsl:text disable-output-escaping="no">IRFaultHandler</xsl:text>
          </VALUE>
        </PARAMETER>
        <PARAMETER>
          <NAME>
            <xsl:text disable-output-escaping="no">p_account_name</xsl:text>
          </NAME>
          <VALUE>
            <xsl:value-of select="."/>
          </VALUE>
        </PARAMETER>
        <PARAMETER>
          <NAME>
            <xsl:text disable-output-escaping="no">p_host_url</xsl:text>
          </NAME>
          <VALUE>
            <xsl:value-of select="concat('http://',$var_ServerDetail/Server_Detail/HostName,':',$var_ServerDetail/Server_Detail/Port,'/')"/>
          </VALUE>
        </PARAMETER>
      </xsl:for-each>
    </G_INPUT>
  </xsl:template>
</xsl:stylesheet>
