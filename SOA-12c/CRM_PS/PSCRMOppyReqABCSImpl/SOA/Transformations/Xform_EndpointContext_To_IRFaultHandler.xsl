<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:oracle-xsl-mapper="http://www.oracle.com/xsl/mapper/schemas" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:oraxsl="http://www.oracle.com/XSL/Transform/java" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:irgsvurl="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetServiceURL" xmlns:ns0="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" exclude-result-prefixes=" xsd oracle-xsl-mapper xsi xsl ns0 mhdr oraext xp20 xref socket dvm oraxsl irgsvurl"
                xmlns:client="http://xmlns.irco.com/XxintCreateOutboundEventV1EsEbiz/IRFaultHandler/IRFaultEmailNotification"
                xmlns:plnk="http://schemas.xmlsoap.org/ws/2003/05/partner-link/"
                xmlns:ns1="http://xmlns.irco.com/EnterpriseObjects/Core/Common/V1"
                xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/"
                xmlns:ns3="http://xmlns.irco.com/IRFaultHandler/IREMFErrorHandler">
   <oracle-xsl-mapper:schema>
      <!--SPECIFICATION OF MAP SOURCES AND TARGETS, DO NOT MODIFY.-->
      <oracle-xsl-mapper:mapSources>
         <oracle-xsl-mapper:source type="WSDL">
            <oracle-xsl-mapper:schema location="oramds:/apps/irsmart/FoundationServiceLibrary/ES_UTILITIES/wsdl/IRFaultEmailNotification.wsdl"/>
            <oracle-xsl-mapper:rootElement name="Security" namespace="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"/>
         </oracle-xsl-mapper:source>
      </oracle-xsl-mapper:mapSources>
      <oracle-xsl-mapper:mapTargets>
         <oracle-xsl-mapper:target type="WSDL">
            <oracle-xsl-mapper:schema location="oramds:/apps/irsmart/FoundationServiceLibrary/ES_UTILITIES/wsdl/IRFaultEmailNotification.wsdl"/>
            <oracle-xsl-mapper:rootElement name="G_INPUT" namespace=""/>
         </oracle-xsl-mapper:target>
      </oracle-xsl-mapper:mapTargets>
      <!--GENERATED BY ORACLE XSL MAPPER 12.2.1.0.0(XSLT Build 151013.0700.0085) AT [TUE NOV 28 15:42:08 IST 2017].-->
   </oracle-xsl-mapper:schema>
   <!--User Editing allowed BELOW this line - DO NOT DELETE THIS LINE-->
   <xsl:template match="/">
      <G_INPUT>
         <xsl:for-each select="/ns0:Security/ns0:UsernameToken/ns0:Username">
            <PARAMETER>
               <NAME>
                  <xsl:text disable-output-escaping="no">p_parent_service_name</xsl:text>
               </NAME>
               <VALUE>
                  <xsl:text disable-output-escaping="no">PSCRMOppyReqABCSImpl</xsl:text>
               </VALUE>
            </PARAMETER>
            <PARAMETER>
               <NAME>
                  <xsl:text disable-output-escaping="no">p_child_service_name</xsl:text>
               </NAME>
               <VALUE>
                  <xsl:text disable-output-escaping="no">IRFaultHandler</xsl:text>
               </VALUE>
            </PARAMETER>
            <PARAMETER>
               <NAME>
                  <xsl:text disable-output-escaping="no">p_account_name</xsl:text>
               </NAME>
               <VALUE>
                  <xsl:value-of select="."/>
               </VALUE>
            </PARAMETER>
         </xsl:for-each>
      </G_INPUT>
   </xsl:template>
</xsl:stylesheet>
