<?xml version = '1.0' encoding = 'UTF-8'?>
<xsl:stylesheet version="1.0" xmlns:client="http://xmlns.irco.com/IRSGSubscriberService" xmlns:getasyncp="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetAsyncParent" xmlns:bpws="http://schemas.xmlsoap.org/ws/2003/03/business-process/" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:irsgreq="http://model/events/schema/XxsgIfaceDetailEO" xmlns:ns0="http://xmlns.irco.com/EnterpriseObjects/Core/Common/V1" xmlns:iruprec="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdatePhaseRec" xmlns:bpm="http://xmlns.oracle.com/bpmn20/extensions" xmlns:iruperr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdateErrorRec" xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:plnk="http://schemas.xmlsoap.org/ws/2003/05/partner-link/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:ora="http://schemas.oracle.com/xpath/extension" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:irsgresp="http://model/events/schema/XxsgIfaceDetailEOResponse" xmlns:ircifr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.CreateIfaceRecord" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:hwf="http://xmlns.oracle.com/bpel/workflow/xpath" xmlns:med="http://schemas.oracle.com/mediator/xpath" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ids="http://xmlns.oracle.com/bpel/services/IdentityService/xpath" xmlns:irgifr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetIfaceRecord" xmlns:iruif="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdateIfaceStatus" xmlns:xdk="http://schemas.oracle.com/bpel/extension/xpath/function/xdk" xmlns:irgsvurl="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetServiceURL" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:ns3="http://schemas.oracle.com/bpel/extension" xmlns:ns1="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:ldap="http://schemas.oracle.com/xpath/extension/ldap" xmlns:tns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" exclude-result-prefixes="xsi xsl client irsgreq ns0 plnk wsdl irsgresp xsd soap12 soap mime tns ns3 ns1 getasyncp bpws xp20 bpel iruprec bpm iruperr ora socket ircifr mhdr oraext dvm hwf med ids irgifr iruif xdk irgsvurl xref ldap">

   <xsl:template match="/">
      <G_INPUT>
         <PARAMETER>
            <NAME>
               <xsl:text disable-output-escaping="no">p_parent_service_name</xsl:text>
            </NAME>
            <VALUE>
               <xsl:text disable-output-escaping="no">CincomInvoiceEDISubscriberService</xsl:text>
            </VALUE>
         </PARAMETER>
         <PARAMETER>
            <NAME>
               <xsl:text disable-output-escaping="no">p_child_service_name</xsl:text>
            </NAME>
            <VALUE>TRANELEXINVOICE810MFTEndpoint</VALUE>
         </PARAMETER>
         <PARAMETER>
            <NAME>
               <xsl:text disable-output-escaping="no">p_account_name</xsl:text>
            </NAME>
            <VALUE>
              <xsl:value-of select="/tns:Security/tns:UsernameToken/tns:Username"/>
            </VALUE>
         </PARAMETER>
      </G_INPUT>
   </xsl:template>
</xsl:stylesheet>