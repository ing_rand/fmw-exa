<?xml version = '1.0' encoding = 'UTF-8'?>
<xsl:stylesheet version="1.0" xmlns:getasyncp="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetAsyncParent" xmlns:bpws="http://schemas.xmlsoap.org/ws/2003/03/business-process/" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns1="http://xmlns.irco.com/AR/warrantyclaimcreditsubmission" xmlns:iruprec="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdatePhaseRec" xmlns:bpm="http://xmlns.oracle.com/bpmn20/extensions" xmlns:iruperr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdateErrorRec" xmlns:plnk="http://schemas.xmlsoap.org/ws/2003/05/partner-link/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:ora="http://schemas.oracle.com/xpath/extension" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:ircifr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.CreateIfaceRecord" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:hwf="http://xmlns.oracle.com/bpel/workflow/xpath" xmlns:med="http://schemas.oracle.com/mediator/xpath" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ids="http://xmlns.oracle.com/bpel/services/IdentityService/xpath" xmlns:irgifr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetIfaceRecord" xmlns:iruif="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdateIfaceStatus" xmlns:xdk="http://schemas.oracle.com/bpel/extension/xpath/function/xdk" xmlns:irgsvurl="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetServiceURL" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" xmlns:client="http://xmlns.oracle.com/TavantClaimCreditProvABCSImpl" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:ns2="http://schemas.oracle.com/bpel/extension" xmlns:ns3="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:ldap="http://schemas.oracle.com/xpath/extension/ldap" exclude-result-prefixes="xsi xsl ns1 plnk wsdl client xsd ns2 ns3 getasyncp bpws xp20 bpel iruprec bpm iruperr ora socket ircifr mhdr oraext dvm hwf med ids irgifr iruif xdk irgsvurl xref ldap" xmlns:oracle-xsl-mapper="http://www.oracle.com/xsl/mapper/schemas">
  <oracle-xsl-mapper:schema>
      <!--SPECIFICATION OF MAP SOURCES AND TARGETS, DO NOT MODIFY.-->
      <oracle-xsl-mapper:mapSources>
         <oracle-xsl-mapper:source type="WSDL">
            <oracle-xsl-mapper:schema location="../TavantClaimCreditProvABCSImpl.wsdl"/>
            <oracle-xsl-mapper:rootElement name="Security" namespace="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"/>
         </oracle-xsl-mapper:source>
      </oracle-xsl-mapper:mapSources>
      <oracle-xsl-mapper:mapTargets>
         <oracle-xsl-mapper:target type="WSDL">
            <oracle-xsl-mapper:schema location="../TavantClaimCreditProvABCSImpl.wsdl"/>
            <oracle-xsl-mapper:rootElement name="G_INPUT" namespace=""/>
         </oracle-xsl-mapper:target>
      </oracle-xsl-mapper:mapTargets>
      <oracle-xsl-mapper:mapShowPrefixes type="true"/>
      <!--GENERATED BY ORACLE XSL MAPPER 12.2.1.0.0(XSLT Build 151013.0700.0085) AT [TUE JUN 14 16:51:12 EDT 2016].-->
   </oracle-xsl-mapper:schema>
   <!--User Editing allowed BELOW this line - DO NOT DELETE THIS LINE-->
   <xsl:template match="/">
    <G_INPUT>
      <PARAMETER>
        <NAME>
          <xsl:text disable-output-escaping="no">p_parent_service_name</xsl:text>
        </NAME>
        <VALUE>
          <xsl:text disable-output-escaping="no">TavantClaimCreditProvABCSImpl</xsl:text>
        </VALUE>
      </PARAMETER>
      <PARAMETER>
        <NAME>
          <xsl:text disable-output-escaping="no">p_child_service_name</xsl:text>
        </NAME>
        <VALUE>
          <xsl:text disable-output-escaping="no">IRFaultHandler</xsl:text>
        </VALUE>
      </PARAMETER>
      <PARAMETER>
        <NAME>
          <xsl:text disable-output-escaping="no">p_account_name</xsl:text>
        </NAME>
        <VALUE>
          <xsl:value-of select="/ns3:Security/ns3:UsernameToken/ns3:Username"/>
        </VALUE>
      </PARAMETER>
    </G_INPUT>
  </xsl:template>
</xsl:stylesheet>