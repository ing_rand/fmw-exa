<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version="1.0" xmlns:oraxsl="http://www.oracle.com/XSL/Transform/java" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:oracle-xsl-mapper="http://www.oracle.com/xsl/mapper/schemas" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:ns0="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="xsd xsi oracle-xsl-mapper xsl ns0 oraxsl xp20 xref mhdr oraext dvm socket" xmlns:ns1="http://xmlns.irco.com/EnterpriseObjects/Core/Common/V1" xmlns:plnk="http://schemas.xmlsoap.org/ws/2003/05/partner-link/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:client="http://xmlns.irco.com/TavantSalesOrderReqABCSImpl/TavantSalesOrderReqABCSImpl" xmlns:irgsvurl="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetServiceURL" xmlns:tvtreq="http://xmlns.irco.com/TavantSalesOrderReqABCSImpl/QueryRequest" xmlns:tvtresp="http://xmlns.irco.com/TavantSalesOrderReqABCSImpl/QueryResponse">
   <oracle-xsl-mapper:schema>
      <oracle-xsl-mapper:mapSources>
         <oracle-xsl-mapper:source type="WSDL">
            <oracle-xsl-mapper:schema location="../WSDLs/TavantSalesOrderReqABCSImpl.wsdl"/>
            <oracle-xsl-mapper:rootElement name="Security" namespace="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"/>
         </oracle-xsl-mapper:source>
      </oracle-xsl-mapper:mapSources>
      <oracle-xsl-mapper:mapTargets>
         <oracle-xsl-mapper:target type="WSDL">
            <oracle-xsl-mapper:schema location="../WSDLs/TavantSalesOrderReqABCSImpl.wsdl"/>
            <oracle-xsl-mapper:rootElement name="G_INPUT" namespace=""/>
         </oracle-xsl-mapper:target>
      </oracle-xsl-mapper:mapTargets>
   </oracle-xsl-mapper:schema>
   <xsl:template match="/">
      <G_INPUT>
         <xsl:for-each select="/ns0:Security/ns0:UsernameToken/ns0:Username">
            <PARAMETER>
               <NAME>
                  <xsl:text disable-output-escaping="no">p_parent_service_name</xsl:text>
               </NAME>
               <VALUE>
                  <xsl:text disable-output-escaping="no">TavantSalesOrderReqABCSImpl</xsl:text>
               </VALUE>
            </PARAMETER>
            <PARAMETER>
               <NAME>
                  <xsl:text disable-output-escaping="no">p_child_service_name</xsl:text>
               </NAME>
               <VALUE>
                  <xsl:text disable-output-escaping="no">MAPICSSalesOrderProvABCSImpl</xsl:text>
               </VALUE>
            </PARAMETER>
            <PARAMETER>
               <NAME>
                  <xsl:text disable-output-escaping="no">p_account_name</xsl:text>
               </NAME>
               <VALUE>
                  <xsl:value-of select="."/>
               </VALUE>
            </PARAMETER>
         </xsl:for-each>
      </G_INPUT>
   </xsl:template>
</xsl:stylesheet>
