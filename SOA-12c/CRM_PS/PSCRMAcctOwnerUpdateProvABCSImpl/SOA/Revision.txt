RiceID : IRMW0238
Rice Name : Update Account Owner Details in PeopleSoft
Composite Name : PSCRMAcctOwnerUpdateProvABCSImpl
Partition Name : CRM_PS
Interaction Type : Synchronous
Environments   : All

Current Release
---------------
Type [Fix/Enhancement/Feature/New] : New
Revision : 1.0
Developer : Reddy Prasad Reddy R
Code Repository [DIMEX/Sharepoint] : DIMEX - ESA:FMW-ESA >> SOA-12c >> CRM_PS
Comments : New Integration with DRF and Cache mechanism to get Dynamic URL from Cache to improve perfomrance.
