<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version="1.0" xmlns:oraxsl="http://www.oracle.com/XSL/Transform/java"
                xmlns:b64="https://github.com/ilyakharlamov/xslt_base64"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:oracle-xsl-mapper="http://www.oracle.com/xsl/mapper/schemas"
                xmlns:getasyncp="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetAsyncParent"
                xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20"
                xmlns:ns1="http://xmlns.irco.com/EcomApiGetModelBOMRequest"
                xmlns:ns3="http://xmlns.irco.com/EnterpriseObjects/Core/Common/V1"
                xmlns:iruprec="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdatePhaseRec"
                xmlns:iruperr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdateErrorRec"
                xmlns:plnk="http://schemas.xmlsoap.org/ws/2003/05/partner-link/"
                xmlns:client="http://xmlns.irco.com/CSGetModelBOMProvABCSImpl"
                xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"
                xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator"
                xmlns:iriifp="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.InsertIfacePayload"
                xmlns:ircifr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.CreateIfaceRecord"
                xmlns:ns2="http://xmlns.irco.com/EcomApiGetModelBOMResponse"
                xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction"
                xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc"
                xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue"
                xmlns:irgifr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetIfaceRecord"
                xmlns:iruif="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdateIfaceStatus"
                xmlns:irgsvurl="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetServiceURL"
                xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions"
                xmlns:ns0="http://www.w3.org/2001/XMLSchema"
                xmlns:ns4="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
                xmlns:irenc="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.Base64Encode"
                xmlns:irdec="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.Base64Decode">
  <oracle-xsl-mapper:schema>
    <oracle-xsl-mapper:mapSources>
      <oracle-xsl-mapper:source type="WSDL">
        <oracle-xsl-mapper:schema location="oramds:/apps/irsmart/FoundationObjectLibrary/schemas/wsse.xsd"/>
        <oracle-xsl-mapper:rootElement name="Security"
                                       namespace="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"/>
      </oracle-xsl-mapper:source>
    </oracle-xsl-mapper:mapSources>
    <oracle-xsl-mapper:mapTargets>
      <oracle-xsl-mapper:target type="WSDL">
        <oracle-xsl-mapper:schema location="oramds:/apps/irsmart/FoundationObjectLibrary/schemas/GetServiceURL.xsd"/>
        <oracle-xsl-mapper:rootElement name="G_INPUT" namespace=""/>
      </oracle-xsl-mapper:target>
    </oracle-xsl-mapper:mapTargets>
  </oracle-xsl-mapper:schema>
  <xsl:template match="/">
    <G_INPUT>
      <xsl:for-each select="/ns4:Security/ns4:UsernameToken/ns4:Username">
        <PARAMETER>
          <NAME>p_parent_service_name</NAME>
          <VALUE>CSGetModelBOMProvABCSImpl</VALUE>
        </PARAMETER>
        <PARAMETER>
          <NAME>p_child_service_name</NAME>
          <VALUE>EcomApi_GetModelBOM</VALUE>
        </PARAMETER>
        <PARAMETER>
          <NAME>p_account_name</NAME>
          <VALUE>
            <xsl:value-of select="."/>
          </VALUE>
        </PARAMETER>
      </xsl:for-each>
    </G_INPUT>
  </xsl:template>
</xsl:stylesheet>
