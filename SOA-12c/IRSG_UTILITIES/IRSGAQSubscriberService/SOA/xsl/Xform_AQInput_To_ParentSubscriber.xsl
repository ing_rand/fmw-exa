<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version="1.0" xmlns:getasyncp="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetAsyncParent" xmlns:bpws="http://schemas.xmlsoap.org/ws/2003/03/business-process/" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns1="http://model/events/schema/XxsgIfaceDetailEO" xmlns:ns0="http://xmlns.irco.com/EnterpriseObjects/Core/Common/V1" xmlns:pc="http://xmlns.oracle.com/pcbpel/" xmlns:iruprec="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdatePhaseRec" xmlns:bpm="http://xmlns.oracle.com/bpmn20/extensions" xmlns:iruperr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdateErrorRec" xmlns:plt="http://schemas.xmlsoap.org/ws/2003/05/partner-link/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:ora="http://schemas.oracle.com/xpath/extension" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:ns2="http://model/events/schema/XxsgIfaceDetailEOResponse" xmlns:ircifr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.CreateIfaceRecord" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:client="http://xmlns.irco.com/DRFGetServiceURL" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:hwf="http://xmlns.oracle.com/bpel/workflow/xpath" xmlns:jca="http://xmlns.oracle.com/pcbpel/wsdl/jca/" xmlns:med="http://schemas.oracle.com/mediator/xpath" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ids="http://xmlns.oracle.com/bpel/services/IdentityService/xpath" xmlns:irgifr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetIfaceRecord" xmlns:iruif="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdateIfaceStatus" xmlns:xdk="http://schemas.oracle.com/bpel/extension/xpath/function/xdk" xmlns:irgsvurl="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetServiceURL" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:tns="http://xmlns.oracle.com/pcbpel/adapter/AQ/p21-sbx/IRSGAQSubscriberService/Fetch_IRSG_Payload_Dtl" xmlns:obj1="http://xmlns.oracle.com/xdb/IRSOA" xmlns:ldap="http://schemas.oracle.com/xpath/extension/ldap" exclude-result-prefixes="xsi xsl pc plt wsdl jca xsd tns obj1 ns1 ns0 ns2 client getasyncp bpws xp20 bpel iruprec bpm iruperr ora socket ircifr mhdr oraext dvm hwf med ids irgifr iruif xdk irgsvurl xref ldap">
   <xsl:template match="/">
      <ns1:IRSGPayLoadEventInfo>
         <ns1:Attribute1>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/ATTRIBUTE1"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:Attribute1>
         <ns1:Attribute10>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/ATTRIBUTE10"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:Attribute10>
         <ns1:Attribute2>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/ATTRIBUTE2"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:Attribute2>
         <ns1:Attribute3>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/ATTRIBUTE3"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:Attribute3>
         <ns1:Attribute4>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/ATTRIBUTE4"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:Attribute4>
         <ns1:Attribute5>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/ATTRIBUTE5"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:Attribute5>
         <ns1:Attribute6>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/ATTRIBUTE6"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:Attribute6>
         <ns1:Attribute7>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/ATTRIBUTE7"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:Attribute7>
         <ns1:Attribute8>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/ATTRIBUTE8"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:Attribute8>
         <ns1:Attribute9>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/ATTRIBUTE9"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:Attribute9>
         <ns1:DestSystemId>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/DEST_SYSTEM_ID"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:DestSystemId>
         <ns1:DocumentId>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/DOCUMENT_ID"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:DocumentId>
         <ns1:ParentDocumentId>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/PARENT_DOCUMENT_ID"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:ParentDocumentId>
         <ns1:Priority>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/PRIORITY"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:Priority>
         <ns1:RiceId>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/RICE_ID"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:RiceId>
         <ns1:RoutingAttribute1>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/ROUTING_ATTRIBUTE1"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:RoutingAttribute1>
         <ns1:RoutingAttribute2>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/ROUTING_ATTRIBUTE2"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:RoutingAttribute2>
         <ns1:RoutingAttribute3>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/ROUTING_ATTRIBUTE3"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:RoutingAttribute3>
         <ns1:RoutingAttribute4>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/ROUTING_ATTRIBUTE4"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:RoutingAttribute4>
         <ns1:RoutingAttribute5>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/ROUTING_ATTRIBUTE5"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:RoutingAttribute5>
         <ns1:SourceKey1>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/SOURCE_KEY1"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:SourceKey1>
         <ns1:SourceKey2>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/SOURCE_KEY2"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:SourceKey2>
         <ns1:SourceKey3>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/SOURCE_KEY3"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:SourceKey3>
         <ns1:SourceKey4>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/SOURCE_KEY4"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:SourceKey4>
         <ns1:SourceKey5>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/SOURCE_KEY5"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:SourceKey5>
         <ns1:SourceSystemId>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/SOURCE_SYSTEM_ID"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:SourceSystemId>
         <ns1:TargetKey1>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/TARGET_KEY1"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:TargetKey1>
         <ns1:TargetKey2>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/TARGET_KEY2"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:TargetKey2>
         <ns1:TargetKey3>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/TARGET_KEY3"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:TargetKey3>
         <ns1:TargetKey4>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/TARGET_KEY4"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:TargetKey4>
         <ns1:TargetKey5>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/TARGET_KEY5"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:TargetKey5>
         <ns1:UserAccount>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/USER_ACCOUNT"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:UserAccount>
         <ns1:FmwInstance>
            <ns1:newValue>
               <xsl:attribute name="value">
                  <xsl:value-of select="/obj1:XXSG_ASYNC_AQ_DOC_TYPE/FMW_INSTANCE"/>
               </xsl:attribute>
            </ns1:newValue>
         </ns1:FmwInstance>
      </ns1:IRSGPayLoadEventInfo>
   </xsl:template>
</xsl:stylesheet>
