<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version="1.0" xmlns:bpws="http://schemas.xmlsoap.org/ws/2003/03/business-process/" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:inp1="http://www.ingerrand.com" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:ns2="http://xmlns.irco.com/EBSDiscreteJobsProvABCSImpl" xmlns:hwf="http://xmlns.oracle.com/bpel/workflow/xpath" xmlns:ns3="http://xmlns.irco.com/EnterpriseObjects/Core/Common/V1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:med="http://schemas.oracle.com/mediator/xpath" xmlns:tns="http://xmlns.oracle.com/XxintUtilities/XxintEventSoapCreateV1EsEbiz/CreateEvent" xmlns:ids="http://xmlns.oracle.com/bpel/services/IdentityService/xpath" xmlns:bpm="http://xmlns.oracle.com/bpmn20/extensions" xmlns:xdk="http://schemas.oracle.com/bpel/extension/xpath/function/xdk" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" xmlns:plnk="http://schemas.xmlsoap.org/ws/2003/05/partner-link/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:ora="http://schemas.oracle.com/xpath/extension" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:ldap="http://schemas.oracle.com/xpath/extension/ldap" exclude-result-prefixes="xsi xsl ns2 ns3 plnk xsd wsdl inp1 tns bpws xp20 mhdr bpel oraext dvm hwf med ids bpm xdk xref ora socket ldap">
   <xsl:param name="varRequestWithoutNameSpace"/>
   <xsl:template match="/">
      <inp1:CreateEventMsg>
         <inp1:XxintEventType>
            <xsl:text disable-output-escaping="no">XXWIP2350_GENERIC_DISC_JOB_OUT</xsl:text>
         </inp1:XxintEventType>
         <inp1:SenderID>
            <xsl:value-of select="/ns2:DiscreteJobInputParameters/ns2:PARTNER_CODE"/>
         </inp1:SenderID>
         <inp1:ReceiverID>
            <xsl:text disable-output-escaping="no">ORACLE</xsl:text>
         </inp1:ReceiverID>
         <inp1:EventPayloadXML>
            <xsl:copy-of select="$varRequestWithoutNameSpace/DiscreteJobInputParameters/ValidateDiscreteJobRequest"/>
         </inp1:EventPayloadXML>
         <inp1:Attribute01>
            <xsl:value-of select="/ns2:DiscreteJobInputParameters/ns2:ValidateDiscreteJobRequest/ns2:OrgCode"/>
         </inp1:Attribute01>
         <inp1:Attribute02>
            <xsl:value-of select="/ns2:DiscreteJobInputParameters/ns2:ValidateDiscreteJobRequest/ns2:StartingJobNumber"/>
         </inp1:Attribute02>
         <inp1:Attribute03>
            <xsl:value-of select="/ns2:DiscreteJobInputParameters/ns2:ValidateDiscreteJobRequest/ns2:EndingJobNumber"/>
         </inp1:Attribute03>
         <inp1:Attribute04>
            <xsl:value-of select="/ns2:DiscreteJobInputParameters/ns2:ValidateDiscreteJobRequest/ns2:JobType"/>
         </inp1:Attribute04>
         <inp1:Attribute05>
            <xsl:value-of select="/ns2:DiscreteJobInputParameters/ns2:ValidateDiscreteJobRequest/ns2:AssemblyItem"/>
         </inp1:Attribute05>
         <inp1:Attribute06>
            <xsl:value-of select="/ns2:DiscreteJobInputParameters/ns2:ValidateDiscreteJobRequest/ns2:ScheduleGroup"/>
         </inp1:Attribute06>
         <inp1:Attribute07>
            <xsl:value-of select="/ns2:DiscreteJobInputParameters/ns2:ValidateDiscreteJobRequest/ns2:StartingBuildSeq"/>
         </inp1:Attribute07>
         <inp1:Attribute08>
            <xsl:value-of select="/ns2:DiscreteJobInputParameters/ns2:ValidateDiscreteJobRequest/ns2:EndingBuildSeq"/>
         </inp1:Attribute08>
         <inp1:Attribute09>
            <xsl:value-of select="/ns2:DiscreteJobInputParameters/ns2:ValidateDiscreteJobRequest/ns2:ClassCode"/>
         </inp1:Attribute09>
         <inp1:Attribute10>
            <xsl:value-of select="/ns2:DiscreteJobInputParameters/ns2:PARTNER_CODE"/>
         </inp1:Attribute10>
         <inp1:Attribute11>
            <xsl:value-of select="/ns2:DiscreteJobInputParameters/ns2:ValidateDiscreteJobRequest/ns2:StartDateS"/>
         </inp1:Attribute11>
         <inp1:Attribute12>
            <xsl:value-of select="/ns2:DiscreteJobInputParameters/ns2:ValidateDiscreteJobRequest/ns2:StartDateE"/>
         </inp1:Attribute12>
         <inp1:Attribute13>
            <xsl:value-of select="/ns2:DiscreteJobInputParameters/ns2:ValidateDiscreteJobRequest/ns2:CompletionDateS"/>
         </inp1:Attribute13>
         <inp1:Attribute14>
            <xsl:value-of select="/ns2:DiscreteJobInputParameters/ns2:ValidateDiscreteJobRequest/ns2:LineCode"/>
         </inp1:Attribute14>
         <inp1:Attribute15>
            <xsl:value-of select="/ns2:DiscreteJobInputParameters/ns2:ValidateDiscreteJobRequest/ns2:SalesOrder"/>
         </inp1:Attribute15>
      </inp1:CreateEventMsg>
   </xsl:template>
</xsl:stylesheet>
