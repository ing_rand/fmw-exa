<?xml version = '1.0' encoding = 'UTF-8'?>
<xsl:stylesheet version="1.0" xmlns:getasyncp="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetAsyncParent" xmlns:bpws="http://schemas.xmlsoap.org/ws/2003/03/business-process/" xmlns:ircoutil="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns1="http://xmlns.irco.com/EnterpriseObjects/Core/Common/V1" xmlns:iruprec="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdatePhaseRec" xmlns:bpm="http://xmlns.oracle.com/bpmn20/extensions" xmlns:iruperr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdateErrorRec" xmlns:plnk="http://schemas.xmlsoap.org/ws/2003/05/partner-link/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:ora="http://schemas.oracle.com/xpath/extension" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:ircifr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.CreateIfaceRecord" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:hwf="http://xmlns.oracle.com/bpel/workflow/xpath" xmlns:med="http://schemas.oracle.com/mediator/xpath" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ids="http://xmlns.oracle.com/bpel/services/IdentityService/xpath" xmlns:irgifr="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetIfaceRecord" xmlns:client="http://xmlns.irco.com/IRFaultHandler/IREMFErrorHandler" xmlns:iruif="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.UpdateIfaceStatus" xmlns:xdk="http://schemas.oracle.com/bpel/extension/xpath/function/xdk" xmlns:irgsvurl="http://www.oracle.com/XSL/Transform/java/com.irco.fmw.utility.GetServiceURL" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:ldap="http://schemas.oracle.com/xpath/extension/ldap" exclude-result-prefixes="xsi xsl ns1 plnk wsdl client xsd getasyncp bpws ircoutil xp20 bpel iruprec bpm iruperr ora socket ircifr mhdr oraext dvm hwf med ids irgifr iruif xdk irgsvurl xref ldap">
   <xsl:template match="/">
      <ns1:IRFault>
         <ns1:FaultMessageID>
            <xsl:value-of select="/ns1:IRFault/ns1:FaultMessageID"/>
         </ns1:FaultMessageID>
         <ns1:FaultMessageSource>
            <xsl:value-of select="/ns1:IRFault/ns1:FaultMessageSource"/>
         </ns1:FaultMessageSource>
         <ns1:FaultReportedDateTime>
            <xsl:value-of select="/ns1:IRFault/ns1:FaultReportedDateTime"/>
         </ns1:FaultReportedDateTime>
         <xsl:if test="/ns1:IRFault/ns1:FaultContext">
            <ns1:FaultContext>
               <ns1:HeaderMessageID>
                  <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderMessageID"/>
               </ns1:HeaderMessageID>
               <ns1:HeaderMessageSource>
                  <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderMessageSource"/>
               </ns1:HeaderMessageSource>
               <ns1:HeaderCreatedDateTime>
                  <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderCreatedDateTime"/>
               </ns1:HeaderCreatedDateTime>
               <ns1:HeaderDetails>
                  <ns1:BusinessInterfaceName>
                     <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:BusinessInterfaceName"/>
                  </ns1:BusinessInterfaceName>
                  <ns1:BusinessTrackingID>
                     <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:BusinessTrackingID"/>
                  </ns1:BusinessTrackingID>
                  <ns1:DocumentType>
                     <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:DocumentType"/>
                  </ns1:DocumentType>
                  <ns1:EnterpriseContextID>
                     <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:EnterpriseContextID"/>
                  </ns1:EnterpriseContextID>
                  <ns1:CompositeName>
                     <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:CompositeName"/>
                  </ns1:CompositeName>
                  <ns1:CompositeRevisionID>
                     <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:CompositeRevisionID"/>
                  </ns1:CompositeRevisionID>
                  <ns1:CompositeInstanceID>
                     <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:CompositeInstanceID"/>
                  </ns1:CompositeInstanceID>
                  <ns1:CompositeCreatedDateTime>
                     <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:CompositeCreatedDateTime"/>
                  </ns1:CompositeCreatedDateTime>
                  <ns1:ComponentName>
                     <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:ComponentName"/>
                  </ns1:ComponentName>
                  <ns1:ComponentInstanceID>
                     <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:ComponentInstanceID"/>
                  </ns1:ComponentInstanceID>
                  <ns1:ComponentType>
                     <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:ComponentType"/>
                  </ns1:ComponentType>
                  <ns1:ComponentCategory>
                     <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:ComponentCategory"/>
                  </ns1:ComponentCategory>
                  <ns1:ComponentTemplate>
                     <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:ComponentTemplate"/>
                  </ns1:ComponentTemplate>
                  <ns1:PartitionName>
                     <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:PartitionName"/>
                  </ns1:PartitionName>
                  <ns1:HostName>
                     <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:HostName"/>
                  </ns1:HostName>
                  <ns1:SenderApplication>
                     <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:SenderApplication"/>
                  </ns1:SenderApplication>
                  <ns1:DestinationApplication>
                     <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:DestinationApplication"/>
                  </ns1:DestinationApplication>
                  <ns1:BusinessKeys>
                     <xsl:for-each select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:BusinessKeys/ns1:Key">
                        <ns1:Key>
                           <ns1:Name>
                              <xsl:value-of select="ns1:Name"/>
                           </ns1:Name>
                           <ns1:Value>
                              <xsl:value-of select="ns1:Value"/>
                           </ns1:Value>
                        </ns1:Key>
                     </xsl:for-each>
                  </ns1:BusinessKeys>
                  <ns1:BusinessCriticality>
                     <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:BusinessCriticality"/>
                  </ns1:BusinessCriticality>
                  <ns1:ComponentEndPointURI>
                     <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:ComponentEndPointURI"/>
                  </ns1:ComponentEndPointURI>
                  <ns1:InboundDocumentPayload>
                     <xsl:value-of select="/ns1:IRFault/ns1:FaultContext/ns1:HeaderDetails/ns1:InboundDocumentPayload"/>
                  </ns1:InboundDocumentPayload>
               </ns1:HeaderDetails>
            </ns1:FaultContext>
         </xsl:if>
         <ns1:FaultFacts>
            <ns1:Category>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultFacts/ns1:Category"/>
            </ns1:Category>
            <ns1:Type>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultFacts/ns1:Type"/>
            </ns1:Type>
            <ns1:ErrorID>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultFacts/ns1:ErrorID"/>
            </ns1:ErrorID>
            <ns1:Severity>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultFacts/ns1:Severity"/>
            </ns1:Severity>
            <ns1:ShortDescription>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultFacts/ns1:ShortDescription"/>
            </ns1:ShortDescription>
            <ns1:LongDescription>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultFacts/ns1:LongDescription"/>
            </ns1:LongDescription>
            <ns1:Code>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultFacts/ns1:Code"/>
            </ns1:Code>
            <ns1:Summary>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultFacts/ns1:Summary"/>
            </ns1:Summary>
            <ns1:Trace>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultFacts/ns1:Trace"/>
            </ns1:Trace>
            <ns1:Note>
               <xsl:value-of select="/ns1:IRFault/ns1:FaultFacts/ns1:Note"/>
            </ns1:Note>
         </ns1:FaultFacts>
      </ns1:IRFault>
   </xsl:template>
</xsl:stylesheet>